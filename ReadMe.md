﻿# EMDSI-lib

## Electric & Magnetic Dipole Superposition Integration Library

This codebase performs the integration, via discrete summations, of weighted orientable electric and magnetic dipoles.  Dipoles use rigorous complete vector forms in the near-field, and include FF forms

This work was originally developed by Josh Philipson the “Near- to Far-Field Transformation for Arbitrarily-Shaped Rotationally-Symmetric Antenna Measurement Surfaces” 



## Getting Started

Copy/clone files and run MAIN.m


## Documentation

```
EMDSI-lib ReadMe.docx
EMDSI_dipole_orientations.pdf
Infinitesimal Dipole Field Transformations - 22 October 2020.docx
Rahmat-Samii on Coordinate Transformations.pdf
```


## Installing

Installation should be trivial. Copy all files into a directory (or clone repo) and open MAIN.m

```
edit/run MAIN.m
```


## Sample Applications

MATLAB

```
MAIN
EMDSI_TEST_1
EMDSI_TEST_2
EMDSI_TEST_3
```



## Built With

* [MATLAB](https://www.mathworks.com/) - The framework used
* [Thesis](https://ruor.uottawa.ca/bitstream/10393/41434/5/Philipson_Joshua_Benjamin_Julius_2020_thesis.pdf) - source Thesis, "Near- to Far-Field Transformation for Arbitrarily-Shaped Rotationally-Symmetric Antenna Measurement Surfaces"
* [Thesis landing page](https://ruor.uottawa.ca/handle/10393/41434) - RUOR landing page for Thesis
* Love!

## Authors

* Josh Philipson
* Derek McNamara

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
 


