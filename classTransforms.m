classdef classTransforms < handle
	%UNTITLED3 Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		
		% From "Usedful Coordinate Transformations for Antenna Applications
		% IEE Trans. on QAntennas and Propagation, VFol. AP-27, No.4, July 1979
		% All angles in radians
		
		
		
		%TX_C2S % cartesian to spherical
		%TX_S2C % spherical to cartesian
		
		%TX_C2CP
		%TX_CP2C
		
		%RotX
		%RotY
		%RotZ

		RAD2DEG=180/pi;
		DEG2RAD=pi/180;
		
		defaultRotPrecision=-15; %10^-n
	end
	   properties (Constant)
		   c = 2.99792458e8;    % m/s
	   end
   
	methods(Static)
		function [x_hat, y_hat, z_hat]=getBasisVector()
			x_hat=[1,0,0].';
			y_hat=[0,1,0].';
			z_hat=[0,0,1].';
		end
		 
		
	
		function [cfigH, caxisH, ctitleH]=labelAxes(x,y,z,titleS, varargin)
			p=inputParser();
			p.addOptional('fontsize', 20);
			p.addOptional('backgroundcolor', 'none');
			p.parse(varargin{:});
			fontsize=p.Results.fontsize;
			backgroundcolor=p.Results.backgroundcolor;
			
			hold on;
			grid on;
			fontsize=20;
			xlabel(x, 'fontsize', fontsize, 'fontweight', 'bold', 'color', 'red', 'backgroundcolor', backgroundcolor);
			ylabel(y, 'fontsize', fontsize, 'fontweight', 'bold', 'color', 'blue', 'backgroundcolor', backgroundcolor);
			zlabel(z, 'fontsize', fontsize, 'fontweight', 'bold', 'color',  'green', 'backgroundcolor', backgroundcolor);
			ctitleH=title(titleS, 'color','b', 'fontsize', fontsize);
			if 0s
				h = rotate3d;
				set(h, 'ActionPreCallback', 'set(gcf,''windowbuttonmotionfcn'',@align_axislabel)')
				set(h, 'ActionPostCallback', 'set(gcf,''windowbuttonmotionfcn'','''')')
				set(gcf, 'ResizeFcn', @align_axislabel)
				align_axislabel([], gca)
				set(gca, 'projection', 'perspective', 'box', 'on')
			end
			view(119, 22);
			cfigH=get(0, 'currentfigure'); %Handle to current figure;
			caxisH=gca;
			
			
			
		end
		
		function h=plotVF(P, VEC, varargin)
			p=inputParser;
			p.addOptional('plotdetails', { 'color', 'r', 'linewidth',1, 'maxheadsize',0.5});
			p.addOptional('displayName', '');
			p.parse(varargin{:});
			plotdetails=p.Results.plotdetails;
			displayName=p.Results.displayName;
			
			%Add vector field
			h=quiver3(P(1), P(2), P(3), VEC(1), VEC(2), VEC(3),plotdetails{:}, 'displayname', displayName);
			
		end
		
		function T=plotPoint(P, varargin)
			%Plot a point of interest in a graph, and create construction lines;
			p=inputParser;
			p.addOptional('ConstructionLines', 1);
			p.addOptional('plotdetails', {'marker', 'o', 'markersize', 10, 'markerfacecolor','g', 'markeredgecolor','k', 'linewidth',2});
			%p.addOptional( 'handlevisibility', 'off');
			p.parse(varargin{:});
			ConstructionLines=p.Results.ConstructionLines;
			plotdetails=p.Results.plotdetails;
			%handlevisibility=p.Results.handlevisibility;
			
			RAD2DEG=180/pi;
			DEG2RAD=pi/180;
			degC=char(176);
			
			Z=zeros(3, 1);
			Px=P(1);
			Py=P(2);
			Pz=P(3);
			T(1)=plot3(P(1), P(2), P(3), plotdetails{:}, 'handlevisibility', 'off');
			
			if ConstructionLines
				%plot construction points if desired;
				grey=[0.494117647058824 0.494117647058824 0.494117647058824];
				plotdetailsConstruct={'marker', 'none', 'color',grey, 'linewidth',0.5, 'linestyle', ':'};
				
				%Vector from origin to the point
				V=[Z,P];
				T(2)=plot3(V(1,:), V(2,:), V(3,:), plotdetailsConstruct{:}, 'handlevisibility', 'off');
				
				%Vector from Point to X,Y plane
				V=[P [Px Py 0].']; %Vector P to projection on XY plane
				T(3)=plot3(V(1,:), V(2,:), V(3,:), plotdetailsConstruct{:}, 'handlevisibility', 'off');
				
				%Vector from Origin to the projection on the X,Y plane
				V=[Z, [Px Py 0].'];
				T(4)=plot3(V(1,:), V(2,:), V(3,:), plotdetailsConstruct{:}, 'handlevisibility', 'off');
				
				%XY projected point to the X axis
				V=[[Px 0 0].' [Px Py 0].'];
				T(5)=plot3(V(1,:), V(2,:), V(3,:), plotdetailsConstruct{:}, 'handlevisibility', 'off');
				
				%XY projected point to the Y axis
				V=[[0 Py 0].' [Px Py 0].'];
				T(6)=plot3(V(1,:), V(2,:), V(3,:), plotdetailsConstruct{:}, 'handlevisibility', 'off');
				
				% projection of point to the z axis
				%V=[[0 0 Pz].' P];
				%plot3(V(1,:), V(2,:), V(3,:), plotdetails{:});
				
				% Annotation of phi angle. +X-axis to the projected point
				%V=[[Px 0 0].' []];
			end
			
			
		end
		
		function []=AugmentTitle(fig, txt)
			figure(fig);
			tt=get(get(gca,'Title'),'String');
			mytext=tt; out='';
			[x,y]=size(mytext);
			for i=1:x
				if i~=1  %&& i~=x
					out=[out char(10)]; %#ok<*AGROW>
				end
				for j=1:y; %#ok<*UNRCH> % length(c)
					out=[out mytext(i,j)];
				end
			end
			tt=out;
			
			wasItHeld=ishold;
			if ~wasItHeld  %If it wasn't held; now hold it
				hold on;
			end
			
			
			fontsize=get(get(gca,'Title'),'fontsize');%Get fontsize
			colorr=get(get(gca, 'Title'), 'color');
			
			
			
			meet=[tt txt];
			set(gca,'Title',text('String',meet, 'fontsize', fontsize, 'color', colorr));
			
			if ~wasItHeld  %If it wasn't held; now hold it
				hold off;
			end
		end
		
		function [F, h]=drawABN(a,b,n, varargin)
			%Return figure handle, and handles to drawn objects
			p=inputParser();
			p.addOptional('theta', nan);
			p.addOptional('linestyle', '-');
			p.addOptional('figureHandle', nan)
			p.addOptional('linewidth', 1);
			p.addOptional('displayname', '');
			p.addOptional('shift', [0;0;0]);
			p.addOptional('sf', 1);
			p.addOptional('colorOverride', 0); % if 1 set to black;
			
			p.parse(varargin{:});
			theta=p.Results.theta;
			figureHandle=p.Results.figureHandle;
			linestyle=p.Results.linestyle;
			linewidth=p.Results.linewidth;
			displayname=p.Results.displayname;
			shift=p.Results.shift;
			sf=p.Results.sf; %Scale factor
			colorOverride = p.Results.colorOverride;
			
			if ~ishandle(figureHandle)
				F=figure;
				grid on;
				hold on;
			else
				%				figure(figureHandle);
				set(0,'CurrentFigure', figureHandle);
			end
			F=figureHandle;
			hold on;
			
			ctr=0;
			if ~colorOverride
				ctr=ctr+1; h(ctr)=plot3([0 a(1)].*sf+shift(1), [0,a(2)].*sf+shift(2), [0,a(3)].*sf+shift(3), '-r', 'linewidth',linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
				ctr=ctr+1; h(ctr)=plot3([0 b(1)].*sf+shift(1), [0,b(2)].*sf+shift(2), [0,b(3)].*sf+shift(3), '-b', 'linewidth', linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
				ctr=ctr+1; h(ctr)=plot3([0 n(1)].*sf+shift(1), [0,n(2)].*sf+shift(2), [0,n(3)].*sf+shift(3), '-g', 'linewidth', linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
			else
				ctr=ctr+1; h(ctr)=plot3([0 a(1)].*sf+shift(1), [0,a(2)].*sf+shift(2), [0,a(3)].*sf+shift(3), '-k', 'linewidth',linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
				ctr=ctr+1; h(ctr)=plot3([0 b(1)].*sf+shift(1), [0,b(2)].*sf+shift(2), [0,b(3)].*sf+shift(3), '-k', 'linewidth', linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
				ctr=ctr+1; h(ctr)=plot3([0 n(1)].*sf+shift(1), [0,n(2)].*sf+shift(2), [0,n(3)].*sf+shift(3), '-k', 'linewidth', linewidth, 'linestyle', linestyle, 'displayname', displayname, 'handlevisibility', 'off');
			end
			axis equal;

			if ~isnan(theta)
				title(['Angle=' num2str(theta)]);
			end
			
			%align_axislabel([], gca)
			
			
			
		end
		
		function [alpha, beta, gamma]=getEulerAngles(xp, yp, zp, varargin)
			% Subfunction to compute the EULER coordinates  (alpha, beta, gamma)
			% given two frames of reference.
			%
			% the WCS, or World coordinate system, also called {c}
			% the LCS, or Local coordinate system, also called {cp}
			%
			% the WCS is assumed to be
			%				x-hat=[1,0,0] = X
			%				y-hat=[0,1,0] = Y
			%				z-hat=[0,0,1] = Z
			%
			% th LCS is assumed to be known, in the WCS, and has it's own
			% cartesian coordinate x-hat-prime, y-hat-prime, z-hat-prime as
			% follows
			%              X1=(X1x, X1y, X1z)
			%              Y1=(Y1x, Y1y, Y1z)
			%              Z1=(Z1x, Z1y, Z1z)
			%
			% ref: http://geom3d.com/data/documents/Calculation=20of=20Euler=20angles.pdf
			%
			% WHAT
			
			p=inputParser();
			p.addOptional('invert', 0);
			p.parse(varargin{:});
			invert=p.Results.invert;
			
			% As it is now; the [alpha, beta, gamma] angles relate/take the LCS to the WCS
			% i.e if you start with the LCS, then serially apply [alpha, beta,
			% gamma], you will attain the WCS.
			% To use this function I will have to INVERT... see note at end
			
			X=[1,0,0]; %Definition of the basis vectors in the WCS
			Y=[0,1,0];
			Z=[0,0,1];
			
			%X1=LCS.X1; %Explicit definition of X1,Y1,Z1, the basis vectors of the LCS, in the WCS
			%Y1=LCS.Y1;
			%Z1=LCS.Z1;
			X1=xp;
			Y1=yp;
			Z1=zp;
			
			X1x=X1(1); X1y=X1(2); X1z=X1(3); % explictly picking up the components
			Y1x=Y1(1); Y1y=Y1(2); Y1z=Y1(3); %#ok<*NASGU>
			Z1x=Z1(1); Z1y=Z1(2); Z1z=Z1(3);
			
			Z1xy=sqrt(Z1x*Z1x+Z1y*Z1y);
			if Z1xy >= eps
				alpha=atan2(Y1x*Z1y-Y1y*Z1x, X1x*Z1y-X1y*Z1x);
				beta=atan2(Z1xy, Z1z);
				gamma=-atan2(-Z1x, Z1y); % Note the sign change here because gamma is inverted in definition from my standard,
			else
				% e.g. only alpha and beta changed. no gamma
				alpha=0;
				if Z1z>0;
					beta=0;
				else
					beta=pi;
				end
				gamma=-atan2(X1y, X1x);
			end
			
			%
			%     I N V E R T
			%
			% If we are looking for the back transformation of the WCS to the LCS,
			% we change signs of angles and exchange alpha with gamma
			%
			%  Like this:
			%		a=-alpha;	 b=-beta;	 g=-gamma;
			%	    alpha=g;	beta=b;		 gamma=a;
			%
			%  OR BETTER, and PREFERABLE..
			%  USE CP2C(alpha,beta,gamma); as this is the desired inverse to go to {c'}
			if invert
				a=-alpha;	 b=-beta;	 g=-gamma;
				alpha=g;	beta=b;		 gamma=a;
			end
			
		end
		
		function [xp,yp,zp]=Rot(R, varargin)
			% Rotate vectors by some rotation matrix
			% Assume that we are rotating the basis vectors, unless explictly
			% passing in values
			
			%p=inputParser;
			%p.addOptional('x', [1,0,0]');
			%p.addOptional('y', [0,1,0]');
			%p.addOptional('z', [0,0,1]');
			%p.parse(varargin{:});
			%x=p.Results.x;
			%y=p.Results.y;
			%z=p.Results.z;
			
			if nargin==1;
				x=[1,0,0].';
				y=[0,1,0].';
				z=[0,0,1].';
			else
				x=varargin{1};
				y=varargin{2};
				z=varargin{3};
			end
			
			xp=R*x;
			yp=R*y;
			zp=R*z;
			
		end
		
		function [r, phi, Z]=cart2pol(x,y,z) %#ok<*INUSL>
			% function [r, phi, Z]=cart2pol(obj,x,y,z)
			% Get Cylindrical Coords, thesis definition
			% from Cartesian Coordinates
			[phi,r, Z]=cart2pol(x,y,z);
		end
		
		function [x, y, z]=pol2cart(r, phi, Z)
			% function [x, y, z]=pol2cart(obj,r, phi, Z)
			% Get Cartesian Coords
			% from Cylindrical Coords (Thesis Definition) RADIANS
			[x,y,z]=pol2cart(phi,r,Z);
		end
		
		function [x,y,z]=sph2cart(r, theta,phi)
			% function [x,y,z]=sph2cart(obj,r, phi, theta)
			% Get Cartesian Coords
			% from Spherical Coords (Professor's definition) RADIANS
			elevation=pi/2-theta;
			[x,y,z]=sph2cart(phi, elevation, r);
			% MATLAB sph2cart needs the elevation angle, and not the +z-hat angle
		end
		
		function [r, theta,phi]=cart2sph(x,y,z )
			% function [r, theta,phi]=cart2sph(obj,x,y,z )
			% Get Spherical Coords
			% from Cartesian Coords (Professor's definition)
			[phi,elevation,r]=cart2sph(x,y,z);
			theta=pi/2-elevation; %i.e. Declination=90-elevation
			% MATLAB sph2cart needs the elevation angle, and not the +z-hat angle
		end
		
		function [er, etheta, ephi, E] = cart2sphvec(r, theta, phi)
			%Return the LCS unit-vector (i.e. local coordinate system) at some spherical point,
			%given by some theta-phi coordinate. This is a wrapper for the
			%cart2sphvec built-in.  Also see: azelaxis.m
			r=[]; %nb; DO NOT NEED RADIUS, but for consistency, will accept 3 coordinates
			t=theta;
			p=phi;
			RAD2DEG=180/pi;
			
			% Josh's Correction
			%er=[cos( pi/2 - t)  .* cos(p), cos(pi/2 -t).*sin(p) , sin(pi/2 - t) ]';
			%etheta=[sin(pi/2-t) .* cos(t), sin(pi/2-t).*sin(p), -cos(pi/2-t)]';
			%ephi= [-sin(t) , cos(p), 0 ]';
			
			% Input Theta=elevation
			%er=[cos( t)  .* cos(p), cos(t).*sin(p) , sin(t) ]';
			%etheta=[sin(t) .* cos(t), sin(t).*sin(p), -cos(t)]';
			%ephi= [-sin(t) , cos(p), 0 ]';
			
			%Use buil-in
			if theta<0; theta=2*pi-abs(theta);end
			if phi>pi; phi=-(2*pi-phi); end
			A = azelaxes(phi*RAD2DEG,theta*RAD2DEG);
			E=A;
			er=A(:,1);
			ephi=A(:,2);
			etheta=A(:,3).*-1;
			
			E=[er, etheta, ephi];
			% Return column vectors er, etheta, ephi, where theta is defined as
			% declination
			% all in radians
			
			% 			roundPrecisionTol=-15; %10.^-15 Round internal representation to this power; for 3D geometry;
			% 			er=roundToPrecision(er,roundPrecisionTol); %Truncate internal expression to  certain digits of precision;
			% 			etheta=roundToPrecision(etheta,roundPrecisionTol); %Truncate internal expression to  certain digits of precision;
			% 			ephi=roundToPrecision(ephi,roundPrecisionTol); %Truncate internal expression to  certain digits of precision;
			% 			E=roundToPrecision(E,roundPrecisionTol); %Truncate internal expression to  certain digits of precision;
			%
			
		end
		
	end
	
	methods
		function obj=classTransforms()
			
		end
		
		function MATRIX=RotX(obj, theta)
			t=theta;
			%obj.RotX=@(t)([1,0,0; 0 cos(t) -sin(t); 0, sin(t), cos(t)]);
			MATRIX=([1,0,0; 0 cos(t) -sin(t); 0, sin(t), cos(t)]);
			n=obj.defaultRotPrecision;
			MATRIX=roundToPrecision(MATRIX, n); %noise on phase plots!
		end
		
		function MATRIX=RotY(obj, theta)
			t=theta;
			%obj.RotY=@(t)([cos(t) 0 sin(t);0 1 0; -sin(t) 0 cos(t)]);
			MATRIX=([cos(t) 0 sin(t);0 1 0; -sin(t) 0 cos(t)]);
			n=obj.defaultRotPrecision;
			MATRIX=roundToPrecision(MATRIX, n); %noise on phase plots!
		end
		
		
		function drawBasisVectors(obj)
			T=obj;
			[X_c, Y_c, Z_c]=T.getBasisVector();	%Basis vectors in {c}
			T.labelAxes('x', 'y', 'z', '', 'fontsize', 26);
			T.drawABN(     X_c,     Y_c,     Z_c ,'figurehandle', gcf, 'linewidth',1, 'linestyle', '-');	% {c}
		end
		
		function MATRIX=RotZ(obj, theta)
			t=theta;
			%obj.RotZ=@(t)([cos(t) -sin(t) 0;sin(t) cos(t) 0; 0 0 1]);
			MATRIX=([cos(t) -sin(t) 0;sin(t) cos(t) 0; 0 0 1]);
			n=obj.defaultRotPrecision;
			MATRIX=roundToPrecision(MATRIX, n); %noise on phase plots!
		end
		
		function [THETA,PHI]=AZEL2THETAPHI(obj, AZ,EL)
			%Input angles assumed to be in DEGREES
			% This function outputs RADIANS
			
			%AZ,EL assumed to be in degrees
			%AZ-EL to Theta, Phi
			% Azimuth is theta
			DEG2RAD=pi/180;
			PHI=AZ*DEG2RAD;
			
			%EL is elevation, we want to return DECLINATION (standardize with
			%Mautz)
			DECL=90-EL;
			DECL=DECL*DEG2RAD;
			THETA=DECL;
		end
		
		function [X,Y,Z]=AZEL2XYZ(obj, AZ,EL, varargin)
			% Input angles assumed to be in DEGREES
			% Return cartesian coordinates for an azimuth and elevation.
			% Assume unit sphere unless passing in 'r' for radius;
			p=inputParser();
			p.addOptional('r', 1); %assume radius is at 1 if we do not specify we are on unit sphere
			p.parse(varargin{:});
			r=p.Results.r;
			[THETA,PHI]=obj.AZEL2THETAPHI(AZ,EL); %got theta phi in radians
			[X,Y,Z]=obj.sph2cart(r,THETA,PHI);
		end
		
		function MATRIX=TX_C2CP(obj, alpha, beta, gamma)
			%transform vectors in {c} to {c'}
			
			a=alpha;
			b=beta;
			g=gamma;
			
			
			MATRIX=[ (cos(g).*cos(a)-sin(g).*cos(b).*sin(a))	,    (cos(g).*sin(a)+sin(g).*cos(b).*cos(a))	,   (sin(g).*sin(b)) ; ...
				(-sin(g).*cos(a)-cos(g).*cos(b).*sin(a))   ,    (-sin(g).*sin(a)+cos(g).*cos(b).*cos(a))   ,   (cos(g).*sin(b))   ; ...
				(sin(b).*sin(a))							,    (-sin(b).*cos(a))							,	(cos(b)) ];
			
		end
		
		function MATRIX=TX_CP2C(obj, alpha, beta, gamma)
			%transform vectors in {c'} to {c}
			MATRIX=transpose(obj.TX_C2CP(alpha, beta, gamma));
			
		end
		
		
		function MATRIX=TX_C2S(obj, theta,phi)
			% Section II Cartesian to Spherical and vice versa
			% Equation 4
			t=theta;
			p=phi;
			MATRIX=		[	(sin(t).*cos(p)), (sin(t).*sin(p)), (cos(t)); ...
							(cos(t).*cos(p)), (cos(t).*sin(p)), (-sin(t)); ...
							(-sin(p)), (cos(p)), ( 0)];
						

		end
		
		function MATRIX=TX_S2C(obj, theta,phi)
			MATRIX=transpose(obj.TX_C2S(theta,phi));
			% Equation 6; Translate Spherical to Cartesian
			%t=theta;
			%p=phi;
			%MATRIX=[(sin(t).*cos(p)), (cos(t).*cos(p)), (-sin(p)) ; ...
			%		(sin(t).*sin(p)), (cos(t).*sin(p)), (cos(p))  ; ...
			%		(cos(t))        , (-sin(t))       , ( 0) ];
			
		end
		
		function [N_HAT, THETA_HAT, PHI_HAT, M]=LCS_SPHERICAL(obj, P)
			% GET LOCAL COORDINATE SYSTEM IN SPHERICAL COORDINATES FOR ANY POINT P
			%We can compute LCS anywhere, but becuase of order-of-operations, I
			%assume a THETA (Declination transform, RotY) anywhere from 0:180, then a
			% change in THETA (azimuth transform, RotZ) anywhere from 0:360.
			%
			% Rotation matrix M will take a point at [1,0,0] and rotate it to P.
			
			TX=obj;
			N=size(P,2); %Number of columns of P give the number of points P.
			
			
			% ALGORITHM TO FIND LCS at a POINT P, or range of points P
			[xo,yo,zo]=TX.getBasisVector;
			
			%Basis vectors known at  R [1,0,0];
			R=[1;0;0];
			[r1, theta1, phi1]=TX.cart2sph(R(1), R(2) , R(3) );
			
			% LCS at R
			r_hat_1=[1;0;0];
			theta_hat_1=[0;0;-1];
			phi_hat_1=[0;1;0];
			
			P=roundToPrecision(P, -15); %Very small residuals less than 1e-17 occur here because of sine/cosine and results in many different LCS near the peak.
			%warning([mfilename ': bypassing roundToPrecision']);
			
			%for i=1:numel(P); P(i,1)=P(i);end%Columnize
			
			for i=1:N
				[r2, theta2, phi2]=TX.cart2sph(P(1,i), P(2,i) , P(3,i) );
				M=TX.RotZ(phi2-phi1)*TX.RotY(theta2-theta1);
				
				% Get new LCS at point P
				r_hat_2=M*r_hat_1;
				theta_hat_2=M*theta_hat_1;
				phi_hat_2=M*phi_hat_1;
				
				N_HAT(:,i)=r_hat_2;
				THETA_HAT(:,i)=theta_hat_2;
				PHI_HAT(:,i)=phi_hat_2;
			end
		end
		
		function plotVec(obj, vec3xN, varargin)
			p=inputParser();
			p.addOptional('plotdetails', {'marker', 'o', 'markersize', 10, 'markerfacecolor','g', 'markeredgecolor','k', 'linewidth',2});
			p.addOptional('figureHandle', gcf);
			p.parse(varargin{:})
			plotdetails=p.Results.plotdetails;
			figureHandle=p.Results.figureHandle;
			
			%Plots a vector of 3xN points
			handle=figureHandle;
			SIN=vec3xN;
			hold on;
			%if ishandle(obj.dataOldHandle)
			%		if strmatch(upper(obj.drawMode), 'OVERWRITE'); %'OVERWRITE' OR 'APPEND'
			%		delete(obj.dataOldHandle);
			%	end
			%end
			h=plot3(SIN(1,:), SIN(2,:), SIN(3,:), plotdetails{:});
		end
		
		function h=plot0Vec(obj, vec3x1, varargin)
			p=inputParser();
			p.addOptional('plotdetails', {'marker', 'none', 'markersize', 10, 'markerfacecolor','g', 'markeredgecolor','k', 'linewidth',2, 'color', 'm'});
			p.addOptional('figureHandle', gcf);
			p.parse(varargin{:})
			plotdetails=p.Results.plotdetails;
			figureHandle=p.Results.figureHandle;
			
			
			x=vec3x1(1);
			y=vec3x1(2);
			z=vec3x1(3);
			
			h(1)=plot3([ 0 x],[ 0 y],[ 0 z], plotdetails{:}, 'linewidth',2);
			h(2)=plot3(x,y,z, 'bd', 'MarkerFaceColor', 'b', 'MarkerSize', 10);
			
			
		end
		
		function drawPlanes(obj)
			hold on;
			%PLANE 1 (XZ)
			A=[-1 0 -1];
			B=[-1 0 1];
			C=[1 0 1];
			D=[1 0 -1];
			
			x=[A(1); B(1); C(1); D(1)];
			y=[A(2); B(2); C(2); D(2)];
			z=[A(3); B(3); C(3); D(3)];
			p=patch(x,y,z,'w', 'handlevisibility', 'off'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
			set(p, 'facelighting', 'none')
			set(p,'facecolor','r')
			set(p,'facealpha',0.1)
			%set(p,'facealpha', 0.5)
% 			axis auto;
			set(p,'specularColorReflectance',0.0)
			text(D(1), D(2), D(3), 'XZ-Plane')
			
			%PLANE 2 (YZ)
			A=[0 -1 -1];
			B=[0 -1 1];
			C=[0 1 1];
			D=[0 1 -1];
			
			x=[A(1); B(1); C(1); D(1)];
			y=[A(2); B(2); C(2); D(2)];
			z=[A(3); B(3); C(3); D(3)];
			p=patch(x,y,z,'w',  'handlevisibility', 'off'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
			set(p, 'facelighting', 'none')
			set(p,'facecolor','b')
			set(p,'facealpha',0.1)
			%set(p,'facealpha', 0.5)
% 			axis auto;
			set(p,'specularColorReflectance',0.0)
			text(D(1), D(2), D(3), 'YZ-Plane')
			
			
			%PLANE 3 (XY)
			A=[-1 -1 0];
			B=[-1 1 0];
			C=[1 1 0];
			D=[1 -1 0];
			
			x=[A(1); B(1); C(1); D(1)];
			y=[A(2); B(2); C(2); D(2)];
			z=[A(3); B(3); C(3); D(3)];
			p=patch(x,y,z,'w',  'handlevisibility', 'off'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
			set(p, 'facelighting', 'none')
			set(p,'facecolor','g')
			set(p,'facealpha',0.1)
			%set(p,'facealpha', 0.5)
% 			axis auto;
			set(p,'specularColorReflectance',0.0)
			text(C(1), C(2),C(3), 'XY-Plane')
		end
		
		
		function sliceView_3Slice(T, opt)
			
			% see: sliceView_Demo.m, MDSI_Sanity_DIPOLE_MAGNETIC_11e
			
			% SLICE 1
			% Simulate Varargin
			ARRAYF=opt.ARRAYF;
			DIPOLES=opt.DIPOLES;
			nDim1=opt.nDim1;
			nDim2=opt.nDim2;
			t_hat=opt.t_hat;
			n_hat=opt.n_hat;
			phi_hat=opt.phi_hat;
			
			opt.SliceArgs={	'ScaleFactor', opt.sliceScaleFactor, 'nDim1', nDim1,'nDim2', nDim2, ...
				'Orientation', 'XY', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
				'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
			fprintf('\nSlice 1..');
			STUFF=PLANE_VECTOR_PLOT_2(opt);
			delete(STUFF.CB);
			
			% SLICE 2
			opt.SliceArgs={	'ScaleFactor', opt.sliceScaleFactor, 'nDim1', nDim1,'nDim2', nDim2, ...
				'Orientation', 'XZ', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
				'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
			fprintf('\nSlice 2..');
			STUFF=PLANE_VECTOR_PLOT_2(opt);
			delete(STUFF.CB);
			
			% SLICE 3
			opt.SliceArgs={	'ScaleFactor', opt.sliceScaleFactor, 'nDim1', nDim1,'nDim2', nDim2, ...
				'Orientation', 'YZ', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
				'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
			fprintf('\nSlice 3..');
			PLANE_VECTOR_PLOT_2(opt);
			
			caxis(opt.cAxis);
		end
		
		function [CLOUD, PLANE]=sliceView_getPointCloud(T, varargin)
			% function [PtCloud, nDim1, nDim2]=sliceView_getPointCloud(T, varargin)
			% Create a 2D slice - return the PointCloud
			% - Define points in a slice
			%					Orientation={'xy', 'xz', 'yz', 'thetaPhi'}
			% - Density in both axes,
			%					Density={nDim1, nDim1 }
			% - Allow scale
			%					ScaleFactor={1};
			% - Allow rotation offsets in
			%					AngleOffset={'dAX', 'dAY', 'dAZ', 'dTheta', 'dPhi'}, applied in this order
			% - Allow offsets in
			%					Offset={'dx', 'dy', 'dz'}, applied in this order
			%
			% SEE: develop_SliceView_PointCloud.m
			
			
			% Simulate Varargin
			%varargin={	'ScaleFactor', 1,'nDim1', 10,'nDim2', 12,'Orientation', 'ThetaPhi', 'dAX', 0, 'dAY', 0, 'dAZ', 0,'dPhi', 10,'dTheta', 0,'dX', 0, 'dY', 0,'dZ', 0.2 };
			
			%% Input Parser
			p=inputParser();
			p.addOptional('Orientation', 'XY');
			p.addOptional('ScaleFactor', 1);
			p.addOptional('nDim1', 10);
			p.addOptional('nDim2', 10);
			p.addOptional('dAX', 0);
			p.addOptional('dAY', 0);
			p.addOptional('dAZ', 0);
			p.addOptional('dTheta',0);
			p.addOptional('dPhi',0);
			p.addOptional('dX', 0);
			p.addOptional('dY', 0);
			p.addOptional('dZ', 0);
			
			p.parse(varargin{:});
			Orientation=p.Results.Orientation; % Orientation
			ScaleFactor=p.Results.ScaleFactor; %Size
			nDim1=p.Results.nDim1; %Density
			nDim2=p.Results.nDim2;
			dAX=p.Results.dAX; % Angles
			dAY=p.Results.dAY;
			dAZ=p.Results.dAZ;
			dPhi=p.Results.dPhi;
			dTheta=p.Results.dTheta;
			dX=p.Results.dX; %Offsets
			dY=p.Results.dY;
			dZ=p.Results.dZ;
			
			% Useful functions
			T=classTransforms();
			flatten_=@(X,nDim1, nDim2)(reshape(X,1,nDim1*nDim2));
			unflatten_=@(X,nDim1,nDim2)(reshape(X, nDim2, nDim1));
			functions.flatten_=flatten_;%return function handles
			functions.unflatten_=unflatten_;
			DEG2RAD=T.DEG2RAD;
			RAD2DEG=T.RAD2DEG;
			
			% SCALE DEFAULT PLANE='xy'
			lin_Dim1=linspace(-ScaleFactor,  ScaleFactor, nDim1);
			lin_Dim2=linspace(-ScaleFactor, ScaleFactor, nDim2);
			[X,Y]=meshgrid(lin_Dim1, lin_Dim2); %size=nDim2 x nDim1
			N=numel(X);
			X=X;
			Y=Y;
			Z=zeros(nDim2, nDim1);
			
			% Small imperfection to see things
			%Z(end)=0.2; Z(1)=-0.2;
			
			X=flatten_(X,nDim1, nDim2);
			Y=flatten_(Y,nDim1, nDim2);
			Z=flatten_(Z,nDim1, nDim2);
			PtCloud=[X;Y;Z];
			clear X Y Z
			
			
			% Set ORIENTATION
			switch upper(Orientation)
				case 'XY'
					% - No rotation needed
					PtCloud=PtCloud;
				case {'XZ', 'THETAPHI'}
					M=T.RotX(-90*DEG2RAD);
					PtCloud=M*PtCloud;
				case 'YZ'
					M=T.RotY(-90*DEG2RAD);
					PtCloud=M*PtCloud;
				otherwise
					error('illegal operation');
			end
			
			% Adjust ANGLE inclination of plane
			% Order of operations is implied by the order below
			M=T.RotY(-DEG2RAD*dTheta)*T.RotZ(DEG2RAD*dPhi)*T.RotZ(DEG2RAD*dAZ)*T.RotY(DEG2RAD*dAY)*T.RotX(DEG2RAD*dAX);
			PtCloud=M*PtCloud;
			
			% Adjust LATERAL offsets, in order below
			PtCloud=bsxfun(@plus, PtCloud, [dX; dY; dZ]); %Add offsets to each dim
			
			roundPrecisionTol=-18; %10.^-15 Round internal representation to this power; for 3D geometry;
			PtCloud=roundToPrecision(PtCloud,roundPrecisionTol); %Truncate internal expression to  certain digits of precision;			
			
			
			CLOUD.PtCloud=PtCloud;
			CLOUD.nDim1=nDim1;
			CLOUD.nDim2=nDim2;
			CLOUD.flatten_=flatten_;%return function handles
			CLOUD.unflatten_=unflatten_;
			
			PLANE.XX=unflatten_(PtCloud(1,:),nDim1, nDim2);
			PLANE.YY=unflatten_(PtCloud(2,:),nDim1, nDim2);
			PLANE.ZZ=unflatten_(PtCloud(3,:),nDim1, nDim2);
			PLANE.nDim1=nDim1;
			PLANE.nDim2=nDim2;
			PLANE.flatten_=flatten_;%return function handles
			PLANE.unflatten_=unflatten_;
		end
		
		function p=sliceView_Patch(T, PLANE)
			
			% Assume a plot is created and we are appending this plane
			hold on;
			XX=PLANE.XX;
			YY=PLANE.YY;
			ZZ=PLANE.ZZ;
			MMAG=PLANE.MAG;
			nDim1=PLANE.nDim1;
			nDim2=PLANE.nDim2;
			
			% PATCH OBJECTS
			% [nDim2, nDim1]=size(XX); %ie Dimensions FLIPPED.
			%						% Crawling this array by rows is like going by
			%						various y-heights, if I assume X,Y data
			% Each patch has 4 points - Bounded by a squre.
			% Each point has 3 dimensions
			x=zeros(4, (nDim1-1)*(nDim2-1));
			y=x;
			z=x;
			ctr=0;
			
			[nRow,nCol]=size(XX); % nRow=nDim2, nCol=nDim1
			
			
			for iR=1:nRow-1 % Along DIM2 (i.e. Y)
				for iC=1:nCol-1 %Along DIM1 (i.e. X)	
					
					%Create 3D A,B,C points in terms of [x y z]' coordinates
					%A=[XX(iR,iC); YY(iR,iC);ZZ(iR,iC)]; %2D "A" point, bottom left in the plane
					%B=[XX(iR,iC+1); YY(iR,iC+1);ZZ(iR,iC+1)]; %3D_"B" point, going ccw looking at the array
					%C=[XX(iR+1,iC+1); YY(iR+1,iC+1);ZZ(iR+1,iC+1)];
					%D=[XX(iR+1,iC); YY(iR+1,iC);ZZ(iR+1,iC)];
					%COLOR(1,ctr)=MMAG(iR,iC); % COLOR at A; Mirror coordinates for "A" point..
					%COLOR(2,ctr)=MMAG(iR,iC+1); %mirror "B" point
					%COLOR(3,ctr)=MMAG(iR+1, iC+1); % "C" point
					%COLOR(4,ctr)=MMAG(iR+1, iC); % "D" point
					
					A=[XX(iR,iC); YY(iR,iC);ZZ(iR,iC)]; %2D "A" point, bottom left in the plane
					B=[XX(iR,iC+1); YY(iR,iC+1);ZZ(iR,iC+1)]; %3D_"B" point, going ccw looking at the array
					C=[XX(iR+1,iC+1); YY(iR+1,iC+1);ZZ(iR+1,iC+1)];
					D=[XX(iR+1,iC); YY(iR+1,iC);ZZ(iR+1,iC)];
					
					ctr=ctr+1;
					x(:,ctr)=[A(1); B(1); C(1); D(1)]; %collect patch points
					y(:,ctr)=[A(2); B(2); C(2); D(2)];
					z(:,ctr)=[A(3); B(3); C(3); D(3)];
					
					COLOR(1,ctr)=MMAG(iR,iC); % COLOR at A; Mirror coordinates for "A" point..
					COLOR(2,ctr)=MMAG(iR,iC+1); %mirror "B" point
					COLOR(3,ctr)=MMAG(iR+1, iC+1); % "C" point
					COLOR(4,ctr)=MMAG(iR+1, iC); % "D" point
					
				end
			end
			
			% PLOT THE OBJECT
			hold on;
			p=patch(x,y,z,COLOR);
			%axis equal;
			grid on;
			
			set(p, 'linestyle', 'none')
			%plot3(XX,YY,ZZ, 'marker', '.', 'linestyle','none', 'color','y'); %Plot dots; diagnostic only
			
			% Post process plots
			set(gcf, 'renderer', 'openGl');
			%set(p, 'Facelighting', 'phong', 'facecolor', 'interp', 'ambientstrength', 0.7)
			%set(p, 'facecolor', 'interp', 'ambientstrength', 1);
			rgb=[192,192,192]./256; %Gray colour
			grid on;
			%axis off; %HIDE AXES
			%set(p,'edgecolor', rgb);
			set(p,'edgecolor', 'none');
			set(p, 'facealpha', 1);
			set(p,'CDataMapping','scaled');
			set(gcf, 'Renderer','zbuffer')
			%set(p, 'edgealpha', 0.2); % THIS LINE CAUSES MASSIVE CRASHING! WHY?
			% light_handle = lightangle(0,45);
			
			
		end
	end
	
end