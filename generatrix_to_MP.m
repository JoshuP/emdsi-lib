function [genNodeMP, dPhi, thetas, phis,genNodeTPMP, genNode_VTP_MP]=generatrix_to_MP(gen_rho, gen_z,M)
%WORKHORSE FUNCTION utility function to take a generatrix to the MP structure

% Jan 2, 2020 - tidy up
% nov 13, 2019; adding; genNode_VTP_MP, which is the corresponding
% (vector-t,phi) array for the generatrix
% recall 't' is already used as an indexxing parameter for the
% generatrix.
% (have generatrix in terms of
%	gen_rho_fit;
%	gen_z_fit;
%	t;


%% TRACER:: Jan 2, 2020
global pp
% pp.TRACER.stack;

T=classTransforms();


%% Revolve generatrix aound M times

NP=numel(gen_rho);
genXZ=[gen_rho; zeros(1,NP); gen_z];
genNode_VTP_MP=[];

genNodeMP=repmat(zeros(3,NP),1,1,M); %Initialize a blank array (of the size of the generatrix 3D cloud on M-pages, preparing to rotate each page by some angle around the azimuth. MP means that it is MultiProd friendly.
phi=linspace(0, 2*pi, M+1);
phi(end)=[]; % Delete the last one (0=2*pi)
dPhi=mean(diff(phi));
for i=1:numel(phi)
	genNodeMP(:,:,i)=multiprod(T.RotZ(phi(i)), genXZ   );
	%Multiply a copy of the generatrix (genXZ) (all 3D points (in XZ plane)) by a given RotZ array.  This is looped for all azimuths.  multiprod multiplies the 3x3 RotZ by the 3xN array. Remember we have
	% N points in the generatrix.  There are M copies conforming to the azimuths that we will rotate by.
end

[x,y,z]=dealMP(genNodeMP,1); %from first plane, want all the x,y,z values
[r,thetas]=T.cart2sph(x,y,z); %export the range of angles
phis=phi; %Discrete Phi values


%% generate genNodeThetaPhi
for j=1:M % for each azimuth
	for i=1:NP %for one latitude at a time
		x=genNodeMP(1,i,j);
		y=genNodeMP(2,i,j);
		z=genNodeMP(3,i,j);
		[~, a, b]=T.cart2sph(x,y,z); 
		 genNodeTPMP(1,i,j)=a; % ThetaPhi Array MP array, useful for Radiation cuts
		 genNodeTPMP(2,i,j)=b;
	end
end


%% Set ambiguity problem at the north (& now, south) pole for
 %phi-vector. Make it the same as at every other latitude, for each
 %given longitude (M, of them)
for i=1:M
	genNodeTPMP(2,1,i)=genNodeTPMP(2,2,i);
	genNodeTPMP(2,end,i)=genNodeTPMP(2,end-1,i);%Sept 23 2019 - did this so PIM_BOR 
end
% warning([mfilename ': added previous line, so first and last points on BOR have same phi' ], 'backtrace', 'off');

% Fix sign problem (taking phi from +/- 180 to only 0-2pi
badIdx=(genNodeTPMP<0);
genNodeTPMP(badIdx)=2*pi+genNodeTPMP(badIdx);


%% genNode_VTP_MP - VECTOR-T, phi MP structure
genNode_VTP_MP=genNodeTPMP;
%replace all the first rows with indices
for i=1:M
	genNode_VTP_MP(1,:,i)=1:NP;
end