function show_GCS_or_LCS(ARRAYP, GCS_or_LCS,  varargin)
%function show_GCS_or_LCS(ARRAYP, GCS_or_LCS,  varargin)
% plot the 3D GCS or LCS - sanity check;

show_ARRAYP(ARRAYP, 'do2D', 0);

p=inputParser();
p.addOptional('interval', 1); %1=plot every one, 2=every second
p.parse(varargin{:});
interval=p.Results.interval;

%Plot LCS
T=classTransforms;
Vsf=.5;
%axis([-1 1 -1 1 -1 1]);
%T.drawPlanes();
%T.drawBasisVectors();
%axis equal;
%view(132,26)
[~,N]=size(ARRAYP);


if isfield(GCS_or_LCS, 'n_hat_MP');
	plotMode='LCS';
	LCS=GCS_or_LCS;
	title('LCS', 'fontsize', 20);
elseif isfield(GCS_or_LCS, 'SPH_R_HAT');
	plotMode='GCS';
	GCS=GCS_or_LCS;
	title('GCS', 'fontsize', 20);
else
	plotMode='OTHERWISE';
end
	
for i=1:interval:N
	%LCS stuff;
	%t   dipole component [X,Y,Z]= [n, phi, t]=[r,b,g]
	%phi dipole component [X,Y,Z]= [t, n, phi]=[r,b,g]
	switch plotMode;
		case 'LCS'
			% CHECK LCS
			n_hat		=LCS.n_hat_MP(:,i);
			phi_hat	=    LCS.phi_hat_MP(:,i);
			t_hat		=LCS.t_hat_MP(:,i);
		case 'GCS'
			% CHECK GCS
			n_hat		=	GCS.SPH_R_HAT(:,i);
			phi_hat	=		GCS.SPH_PHI_HAT(:,i);
			t_hat		=	GCS.SPH_THETA_HAT(:,i);
		otherwise
			error('ya gotta use LCS or GCS');
	end
	
	P=ARRAYP(:,i);
	%Color code for t-dipole component
	T.plotVF(P, phi_hat*Vsf, 'plotdetails', { 'color', 'b', 'maxheadsize',2.5, 'linewidth',2});
	T.plotVF(P, n_hat*Vsf, 'plotdetails', { 'color', 'r', 'maxheadsize',2.5, 'linewidth',2});
	T.plotVF(P, t_hat*Vsf, 'plotdetails', { 'color', 'g', 'maxheadsize',2.5, 'linewidth',2});
	%drawnow %BREAKPOINT here to see ordering of the plots;
end