function [gen_rho_fit, gen_z_fit, t, PIM_BOR]=parametericInterpolateGeneratrix(gen_rho, gen_z, varargin)
%%

% EXACT POINTS FROM MASTER_GENERATRIX in 
%		[t, gen_rho, gen_z, sph_r, sph_theta, dx, ddx, facet_dx, facet_ddx]
%
% CONTINUOUS-INTERPOLANTS for the following (as a f(t))
%		[t, rho, z, sph_r, sph_theta, 



%%
p=inputParser();
p.addOptional('FITTYPE', 'spline');
p.addOptional('t_begin',1);
p.addOptional('t_end', numel(gen_z));
p.addOptional('doDistance',0); % by default do not worry about arc lengths?
p.addOptional('dx',[]); %Pass in the cumulative sum; I will recompute the deltas. This is along the arc length precisely.

p.parse(varargin{:});
FITTYPE=p.Results.FITTYPE;
t_begin=p.Results.t_begin;
t_end=p.Results.t_end;
doDistance=p.Results.doDistance;
dx=p.Results.dx; %distance since beginning;

%% TRACER:: Jan 2, 2020
global pp
% pp.TRACER.stack;


%%


%t=1:numel(gen_z);				% The t-"Parameter" (index along generatrix, integer for original BOR)
t=linspace(t_begin, t_end, numel(gen_z));% Jan 1, change to go between t_begin and t_end. By default: Integer values are the points I directly specified.

% Parametric fits
% parametric fit, based on : spline  smoothingspline,  poly7, pchipinterp, splineinterp
%ft = fittype( 'splineinterp' );		 %ORIGINAL
ft = fittype( FITTYPE);		

% Eliminate warnings from prepareCurveData in case rows & columns are input together
t=setVectorOrientation(t,'orientation', 'row');
gen_rho=setVectorOrientation(gen_rho,'orientation', 'row');
gen_z=setVectorOrientation(gen_z,'orientation', 'row');


[xData, yData] = prepareCurveData( t, gen_rho ); [gen_rho_fit, ~] = fit( xData, yData, ft );	% gen_rho
[xData, yData] = prepareCurveData( t, gen_z );   [gen_z_fit, ~] = fit( xData, yData, ft );		% gen_z 

PIM_BOR.generatrix.cfit_t=t;
PIM_BOR.generatrix.cfit_rho=gen_rho_fit;
PIM_BOR.generatrix.cfit_z=gen_z_fit;



%% SPHERICAL COORDINATES (Jan 1)
% generatrix::spherical coordinates 
T=classTransforms;

[gen_r, gen_theta, gen_phi] =T.cart2sph(gen_rho, zeros(size(gen_rho)), gen_z); % - omit phi-fit .. we are on prime-rib
% generatrix::spherical coordinates interpolator
[xData, yData] = prepareCurveData( t, gen_r);		[cfit_sph_r		, ~] = fit( xData, yData, ft );	% gen_rho
[xData, yData] = prepareCurveData( t, gen_theta );	[cfit_sph_theta	, ~] = fit( xData, yData, ft );	% gen_theta
[xData, yData] = prepareCurveData( t, gen_phi );	[cfit_sph_phi	, ~] = fit( xData, yData, ft );	% gen_theta
PIM_BOR.generatrix.cfit_sph_r		= cfit_sph_r;
PIM_BOR.generatrix.cfit_sph_theta	= cfit_sph_theta;
% PIM_BOR.generatrix.cfit_sph_phi		= cfit_sph_phi; %<--[[Omit this - we are on prime rib.


%% condition PIM BOR
PIM_BOR.t=t;
PIM_BOR.gen_rho=gen_rho;  % - generatrix
PIM_BOR.gen_z=gen_z;

PIM_BOR.sph_r=gen_r;		% - corresponding spherical coordinates
PIM_BOR.sph_theta=gen_theta;
% PIM_BOR.sph_phi=gen_phi; %OMIT


%% Do distance along curve?
if doDistance
	
	% ALONG ARC
	PIM_BOR.dx=dx;  %Absolute length
	PIM_BOR.ddx=[0, diff(dx)]; % Space between points
	
	% Interpolator (ALONG ARC)
	[xData, yData] = prepareCurveData( t, dx);	[cfit_dx, ~] = fit( xData, yData, ft );	% cumulative distance along survace (ddx does not have meaning for interpolator)
	PIM_BOR.generatrix.cfit_dx	= cfit_dx;
	
	% ALONG FACETS (FLAT<<< no smooth interpolations between generatrix
	% points
	facet_ddx=sqrt(diff(gen_rho).^2+diff(gen_z).^2); %distance between points on the master generatrix - do not assume curved ('splinefit')
	facet_dx=cumsum(facet_ddx); %
	PIM_BOR.facet_dx=[0 facet_dx];
	PIM_BOR.facet_ddx=[0 facet_ddx];
else

end

end