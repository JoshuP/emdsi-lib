classdef class_child_BOR < handle
	% standard BOR drawn from an instance of class_BOR_Master
	%
	% refs: 
	%		class_BOR_Master
	%		class_BOR_lib
	%		init_classGlobalPassPort
	%		define_BOR_Surface
	%		generatrix_to_MP
	
	properties
		BOR_TYPE
		masterBOR
		name
		
		NP
		M
		t 
		
		%% - _MP, 3D related
		D3 
		% LCS
		% GCS
		% MP
		% TNP
		% VTP
		% thetas
		% phis
		
		%% - Generatrix related
		D2 
		% t
		% PIM_BOR <-- special - linearFit on 'Faceted'-BOR
		% gen_rho
		% gen_z
		% sph_r
		% sph_theta
		% dx
		% ddx
		% facet_dx
		% facet_ddx
		% LCS
		% GCS
		
		%% Structure from master_BOR (class_BOR_Master) that I am following		
		%              t: [1x100 double]
		%        gen_rho: [1x100 double]
		%          gen_z: [1x100 double]
		%          sph_r: [1x100 double]
		%      sph_theta: [1x100 double]
		%             dx: [1x100 double]
		%            ddx: [1x100 double]
		%       facet_dx: [1x100 double]
		%      facet_ddx: [1x100 double]
		%            LCS: [1x1 struct]
		%            GCS: [1x1 struct]
	end
	
	methods
		function obj=class_child_BOR(master_BOR, NP, M, varargin)
			
			global pp
			
			p=inputParser();
			p.addOptional('sampled_BOR_type', 'make_std_BOR'); %' make_std_BOR,  make_FFT_Node_BOR, make_FFT_Span_BOR
			p.parse(varargin{:});
			sampled_BOR_type=p.Results.sampled_BOR_type;
			
			obj.masterBOR=master_BOR;
			obj.NP=NP;
			obj.M=M;
			obj.BOR_TYPE=master_BOR.BOR_TYPE;
			
			switch sampled_BOR_type
				case {'make_std_BOR', 'make_FFT_Node_BOR', 'make_FFT_Span_BOR'}
					%pre_mode=pp.TRACER.mode; %eqab_comment
					%if pre_mode~=0; pp.TRACER.mode=0; end%eqab_comment
					
					obj.make_std_BOR(NP,M, sampled_BOR_type); %May move this & explicitly call
					
					%if pre_mode~=0; pp.TRACER.mode=pre_mode; end%eqab_comment
				otherwise
					warning([mfilename ': you will have to make_std_BOR or otherwise actually create it. BLANK BOR atm..']);
			end
		end
		
		function make_std_BOR(obj, NP, M, sampled_BOR_type)
			% We specified the master, and NP, and M, 
			% Given a master_BOR (class instance of class_BOR_Master), 
			% find NP points equidistantly spaced upon the generatrix, 
			% and compute the LCS, GCS on it and revolved versions, Mx
			%
			% the indexxing parameter 't' matches up with the instance of
			% class_BOR_Master upon which it is generated
			masterBOR=obj.masterBOR;
			if masterBOR.NP<NP
				warning([mfilename ': does not make much sense to have BOR-child with more points in it than the master_BOR']);
			end
			
			
			switch sampled_BOR_type
				case {'make_std_BOR', 'make_FFT_Node_BOR'};
					
					% simply interpolate on the masterBOR
					% master_BOR was already equidistantly chopped and indexxed, let us
					% rely on that, and simply re-sample the (already linearized)
					% t-parameter
					ti=linspace(masterBOR.PIM_BOR.t(1), masterBOR.PIM_BOR.t(end),NP);
					
					generatrix=masterBOR.PIM_BOR.generatrix;
					t			= ti; %% -----------[ NOTE! t is redefined for t-locally
					
% 				case 'make_FFT_Node_BOR' % ' make_std_BOR,  make_FFT_Node_BOR, make_FFT_Span_BOR
% 					% same as notes below, but in this case we will have exact thetas as
% 					% on the current MH_BOR_IN, but will have new thetas.
% 					%
% 					% flat-facet interpolation mode
% 					%
% 					% There was a judgement call here.
% 					% Perhaps this could be 'make_std_BOR', and re-interpolate at regular
% 					% poingt
% 					
% 					parentBOR			= obj.masterBOR; %which is derived from a masterBOR
% 					generatrix_facet	= parentBOR.D2.PIM_BOR.generatrix; %2D interpolator, for linear-interp based on nodes in MH
% 					t					= generatrix_facet.cfit_t; %Indices where the faceted MH bor exist, as sampled off of masterBOR
% 					ti					= t; %These are the exact locations on generatrix that give (RH, ZH)
% 										
% 					gen_rho_span=generatrix_facet.cfit_rho(ti).';
% 					gen_z_span=generatrix_facet.cfit_z(ti).';
% 					
% 					% Sanity check ; the gen_z_span ought to be the same as if I looked at
% 					% the half-way z-inbetween each level
% 					%  gen_rho=parentBOR.D2.gen_rho
% 					%  gen_z=parentBOR.D2.gen_z
% 					%  check_gen_z=gen_z(1:end-1)+diff(gen_z)/2;
% 					%  sum(gen_z_span-check_gen_z); %And they are identical if sum 10E-15 or so
% 					generatrix=generatrix_facet;
					
				case 'make_FFT_Span_BOR'
					% Creates the BOR for the MH Excitation integral, that has the
					% exact locations required by the FFT.
					%
					% This local-parent is the 'MH_BOR_IN' instance of class_child_BOR
					% It is the (ie) 21-element array.
					% This childBOR is derived from the S_measure master bor (pp.BORlib_Smes.masterBOR)
					%
					% The theta-values are exactly half-way between the nodes of
					% the 'local parent bor, MH_BOR_IN'.  Values are interpolated using
					% the 'flat-facet interpolation' directly in between the nodes,
					% and *not* the smooth masterBOR surface.
					%
					% The phi-values are the locations as set by the FFT, which are in
					% fact recomputed here in the regular way. I have confirmed that the
					% PHI values via FFT are identical to the phi values computed in the
					% linspace way
					%
					% flat-facet interpolation mode
					
					parentBOR			= obj.masterBOR; %which is derived from a masterBOR
					generatrix_facet	= parentBOR.D2.PIM_BOR.generatrix; %2D interpolator, for linear-interp based on nodes in MH
					t					= generatrix_facet.cfit_t; %Indices where the faceted MH bor exist, as sampled off of masterBOR
					ti					= t(1:end-1)+diff(t)/2; %These are the locations half-way in-between, corresponding to (z_i + z_i+1)/2
					NP=numel(ti);
					obj.NP=NP; %  For SPAN need one less points
					M=M; 
					%resample based on faceted flat faces
					
					gen_rho_span=generatrix_facet.cfit_rho(ti).';
					gen_z_span=generatrix_facet.cfit_z(ti).';
					
					% Sanity check ; the gen_z_span ought to be the same as if I looked at
					% the half-way z-inbetween each level
					%  gen_rho=parentBOR.D2.gen_rho
					%  gen_z=parentBOR.D2.gen_z
					%  check_gen_z=gen_z(1:end-1)+diff(gen_z)/2;
					%  sum(gen_z_span-check_gen_z); %And they are identical if sum 10E-15 or so
					generatrix=generatrix_facet;
				otherwise
					error([mfilename ': what kind of masterBOR are you trying to pass off here?']);
			end
			
			T=classTransforms;
			%--------------------------------------------------------------
			%--       Maths on 2D generator curve first				     --
			%--------------------------------------------------------------
			gen_rho = generatrix.cfit_rho(ti).';
			gen_z   = generatrix.cfit_z(ti).';
			sph_r	= generatrix.cfit_sph_r(ti).';
			sph_theta=generatrix.cfit_sph_theta(ti).';
			dx		= generatrix.cfit_dx(ti).'; %cumulative distance along the surface at indices to ti
			ddx		= [0 diff(dx)]; %delta-distance between points along curve
			
			
			% ALONG FACETS (FLAT<<< no smooth interpolations between generatrix  points
			% see method in: parametericInterpolateGeneratrix.m
			facet_ddx=sqrt(diff(gen_rho).^2+diff(gen_z).^2); %distance between points on the master generatrix - do not assume curved ('splinefit')
			facet_dx=cumsum(facet_ddx); %
			facet_dx=[0 facet_dx];
			facet_ddx=[0 facet_ddx];

			% Store Results for (local) generator curve (subset)
			obj.t=ti; % duplicates, but highlights the exact [t_subset] from master
			obj.D2.t=ti;
			obj.D2.gen_rho=gen_rho;
			obj.D2.gen_z=gen_z;
			obj.D2.sph_r=sph_r;
			obj.D2.sph_theta=sph_theta;
			obj.D2.dx=dx;
			obj.D2.ddx=ddx;
			obj.D2.facet_dx=facet_dx;
			obj.D2.facet_ddx=facet_ddx;
		
			% GCS and LCS calls BOR_master interpolation method	(subset)
			switch sampled_BOR_type; % {'make_std_BOR', 'make_FFT_Node_BOR', 'make_FFT_Span_BOR'}
				case {'make_std_BOR', 'make_FFT_Node_BOR'}
				 % smooth interpolation from master_BOR instance
					[LCS_D2, GCS_D2]=masterBOR.get_GCS_LCS(ti); % t=indices to get the GCS & LCS
				case {'make_FFT_Span_BOR'} %flat-facet interpolation from child_BOR instance, here we have to get the master of the master to draw accurate LCS from the masterBOR geometry
					[LCS_D2, GCS_D2]=masterBOR.masterBOR.get_GCS_LCS(ti); % t=indices to get the GCS & LCS
			end
			obj.D2.LCS=LCS_D2;
			obj.D2.GCS=GCS_D2;
			
			% Faceted-generatrix, map the ti(subset) to the entire-span (t(1):t(end)
			FITTYPE='linearinterp'; %'poly1'; % 'linearinterp'=Linear piecewise curve
			[~, ~, ~, PIM_BOR]=parametericInterpolateGeneratrix(gen_rho, gen_z,'doDistance', 1 ,'dx', dx,'FITTYPE',FITTYPE, ...
				't_begin', t(1), 't_end', t(end)); %create interpolators based on 'index' position in generator,
			obj.D2.PIM_BOR=PIM_BOR;
			
			%--------------------------------------------------------------
			%--       3D (MP)   generator curve last				     --
			%--------------------------------------------------------------
			% Take the generator curve and revolve it around the points where we
			% need this. 
			% ref: generatrix_to_MP.m
			
			% get thetas and phis pertaining to the generatrix
			[genNodeMP, dPhi, thetas, phis,genNodeTPMP, genNode_VTP_MP]=generatrix_to_MP(gen_rho, gen_z,M); 
			
			% get LCS and GCS _MP array, (NB: Each phi computes all GCS/LCS at a time)
			for i=1:numel(phis)
				
				LCS_MP.n_hat_MP(:,:,i)	= multiprod(T.RotZ(phis(i)),   LCS_D2.n_hat_MP(:,:)   );
				LCS_MP.t_hat_MP(:,:,i)	= multiprod(T.RotZ(phis(i)),   LCS_D2.t_hat_MP(:,:)   );
				LCS_MP.phi_hat_MP(:,:,i)= multiprod(T.RotZ(phis(i)), LCS_D2.phi_hat_MP(:,:)   );
				
				GCS_MP.SPH_R_HAT(:,:,i)		= multiprod(T.RotZ(phis(i)),       GCS_D2.SPH_R_HAT(:,:)   );
				GCS_MP.SPH_THETA_HAT(:,:,i)	= multiprod(T.RotZ(phis(i)),   GCS_D2.SPH_THETA_HAT(:,:)   );
				GCS_MP.SPH_PHI_HAT(:,:,i)	= multiprod(T.RotZ(phis(i)),     GCS_D2.SPH_PHI_HAT(:,:)   );
				
				%Multiply a copy of the generatrix LCS/GCS (all 3D points (in XZ plane)) by a given RotZ array.  
				% This is looped for all azimuths.  multiprod multiplies the 3x3 RotZ by the 3xN array. Remember we have
				% N points in the generatrix.  There are M copies conforming to the azimuths that we will rotate by.
			end
			
			if 0 % optional visualize of the LCS
				LCS=LCS_MP;
				ARRAYP=reshape_flatten_MP(genNodeMP, 3, NP*M, 1);
				show_GCS_or_LCS(ARRAYP, LCS, 'interval', 1);  % LCS vectors in 3D
				% show_GCS_or_LCS(ARRAYP, GCS, 'interval', 20);  % GCS vectors in 3D
			end
						
			% Store results for (MP) structure (subset)
			obj.D3.genNode_MP_XYZ=genNodeMP;
			obj.D3.genNode_2D_XYZ=reshape_flatten_MP(genNodeMP, 3, NP*M, 1);
			
			obj.D3.genNode_MP_TNP=genNodeTPMP;
			obj.D3.genNode_VTP_MP=genNode_VTP_MP;
			
			obj.D3.genNode_MP_SphLCS=GCS_MP;
			obj.D3.genNode_MP_BorLCS=LCS_MP;
			obj.D3.thetas=thetas;
			obj.D3.phis=phis;
			
			
		end
		% MH_BOR_NODE_FFT
		% MH_BOR_SPAN_FFT
		
		function MH_BOR_SPAN_FFT(obj, NP, M)
			% Creates the BOR for the MH Excitation integral, that has the 
			% exact locations required by the FFT.
			%
			% This local-parent is the 'MH_BOR_IN' instance of class_child_BOR 
			% It is the (ie) 21-element array.
			% This childBOR is derived from the S_measure master bor (pp.BORlib_Smes.masterBOR)
			%
			% The theta-values are exactly half-way between the nodes of 
			% the 'local parent bor, MH_BOR_IN'.  Values are interpolated using
			% the 'flat-facet interpolation' directly in between the nodes, 
			% and *not* the smooth masterBOR surface.
			%
			% The phi-values are the locations as set by the FFT, which are in
			% fact recomputed here in the regular way. I have confirmed that the
			% PHI values via FFT are identical to the phi values computed in the
			% linspace way
			%			
			T=classTransforms;
			
			% Maths along one generatrix
			parentBOR			= obj.masterBOR; %which is derived from a masterBOR
			generatrix_facet	= parentBOR.D2.PIM_BOR.generatrix; %2D interpolator, for linear-interp based on nodes in MH
			t					= generatrix_facet.cfit_t; %Indices where the faceted MH bor exist, as sampled off of masterBOR
			ti					= t(1:end-1)+diff(t)/2; %These are the locations half-way in-between, corresponding to (z_i + z_i+1)/2
			%resample based on faceted flat faces
			
			gen_rho_span=generatrix_facet.cfit_rho(ti).';
			gen_z_span=generatrix_facet.cfit_z(ti).';
			
			% Sanity check ; the gen_z_span ought to be the same as if I looked at
			% the half-way z-inbetween each level
			%  gen_rho=parentBOR.D2.gen_rho
			%  gen_z=parentBOR.D2.gen_z
			%  check_gen_z=gen_z(1:end-1)+diff(gen_z)/2;
			%  sum(gen_z_span-check_gen_z); %And they are identical if sum 10E-15 or so
			
			obj.NP=numel(ti);
			obj.M=M;
			obj.t=ti;
			% 2D generatrix
			
			%--------------------------------------------------------------
			%--       Maths on 2D generator curve first				     --
			%--------------------------------------------------------------
			generatrix=generatrix_facet;
			%t=ti %% -----------[ NOTE! t is redefined for t-locally up above, in-between 'z'-vals
			
			gen_rho = generatrix.cfit_rho(ti).';
			gen_z   = generatrix.cfit_z(ti).';
			sph_r	= generatrix.cfit_sph_r(ti).';
			sph_theta=generatrix.cfit_sph_theta(ti).';
			dx		= generatrix.cfit_dx(ti).'; %cumulative distance along the surface at indices to ti
			ddx		= [0 diff(dx)]; %delta-distance between points along curve
			
			
			% ALONG FACETS (FLAT<<< no smooth interpolations between generatrix  points
			% see method in: parametericInterpolateGeneratrix.m
			facet_ddx=sqrt(diff(gen_rho).^2+diff(gen_z).^2); %distance between points on the master generatrix - do not assume curved ('splinefit')
			facet_dx=cumsum(facet_ddx); %
			facet_dx=[0 facet_dx];
			facet_ddx=[0 facet_ddx];
			
			% Store Results for (local) generator curve (subset)
			obj.t=ti; % duplicates, but highlights the exact [t_subset] from master
			obj.D2.t=ti;
			obj.D2.gen_rho=gen_rho;
			obj.D2.gen_z=gen_z;
			obj.D2.sph_r=sph_r;
			obj.D2.sph_theta=sph_theta;
			obj.D2.dx=dx;
			obj.D2.ddx=ddx;
			obj.D2.facet_dx=facet_dx;
			obj.D2.facet_ddx=facet_ddx;
			
			% GCS and LCS calls BOR_master interpolation method	(subset)
			[LCS_D2, GCS_D2]=parentBOR.masterBOR.get_GCS_LCS(ti); % t=indices to get the GCS & LCS
			obj.D2.LCS=LCS_D2;
			obj.D2.GCS=GCS_D2;
			
			% Faceted-generatrix, map the ti(subset) to the entire-span (t(1):t(end)
			FITTYPE='linearinterp'; %'poly1'; % 'linearinterp'=Linear piecewise curve
			[~, ~, ~, PIM_BOR]=parametericInterpolateGeneratrix(gen_rho, gen_z,'doDistance', 1 ,'dx', dx,'FITTYPE',FITTYPE, ...
				't_begin', t(1), 't_end', t(end)); %create interpolators based on 'index' position in generator,
			obj.D2.PIM_BOR=PIM_BOR;
			
			%--------------------------------------------------------------
			%--       3D (MP)   generator curve last				     --
			%--------------------------------------------------------------
			% Take the generator curve and revolve it around the points where we
			% need this.
			% ref: generatrix_to_MP.m
			
			% get thetas and phis pertaining to the generatrix
			[genNodeMP, dPhi, thetas, phis,genNodeTPMP, genNode_VTP_MP]=generatrix_to_MP(gen_rho, gen_z,M);
			
			% get LCS and GCS _MP array, (NB: Each phi computes all GCS/LCS at a time)
			for i=1:numel(phis)
				
				LCS_MP.n_hat_MP(:,:,i)	= multiprod(T.RotZ(phis(i)),   LCS_D2.n_hat_MP(:,:)   );
				LCS_MP.t_hat_MP(:,:,i)	= multiprod(T.RotZ(phis(i)),   LCS_D2.t_hat_MP(:,:)   );
				LCS_MP.phi_hat_MP(:,:,i)= multiprod(T.RotZ(phis(i)), LCS_D2.phi_hat_MP(:,:)   );
				
				GCS_MP.SPH_R_HAT(:,:,i)		= multiprod(T.RotZ(phis(i)),       GCS_D2.SPH_R_HAT(:,:)   );
				GCS_MP.SPH_THETA_HAT(:,:,i)	= multiprod(T.RotZ(phis(i)),   GCS_D2.SPH_THETA_HAT(:,:)   );
				GCS_MP.SPH_PHI_HAT(:,:,i)	= multiprod(T.RotZ(phis(i)),     GCS_D2.SPH_PHI_HAT(:,:)   );
				
				%Multiply a copy of the generatrix LCS/GCS (all 3D points (in XZ plane)) by a given RotZ array.
				% This is looped for all azimuths.  multiprod multiplies the 3x3 RotZ by the 3xN array. Remember we have
				% N points in the generatrix.  There are M copies conforming to the azimuths that we will rotate by.
			end
			
			if 0 % optional visualize of the LCS
				LCS=LCS_MP;
				ARRAYP=reshape_flatten_MP(genNodeMP, 3, NP*M, 1);
				show_GCS_or_LCS(ARRAYP, LCS, 'interval', 1);  % LCS vectors in 3D
				% show_GCS_or_LCS(ARRAYP, GCS, 'interval', 20);  % GCS vectors in 3D
			end
			
			% Store results for (MP) structure (subset)
			obj.D3.genNode_MP_XYZ=genNodeMP;
			obj.D3.genNode_2D_XYZ=reshape_flatten_MP(genNodeMP, 3, NP*M, 1);
			
			obj.D3.genNode_MP_TNP=genNodeTPMP;
			obj.D3.genNode_VTP_MP=genNode_VTP_MP;
			
			obj.D3.genNode_MP_SphLCS=GCS_MP;
			obj.D3.genNode_MP_BorLCS=LCS_MP;
			obj.D3.thetas=thetas;
			obj.D3.phis=phis;
		end
		
			
		function [genNodeMP, thetas, phis, genNodeTPMP, PIM_BOR, genNode_VTP_MP, gen_rho, gen_z, BOR_LCS_NF, SPH_LCS_NF]=interface(obj, varargin)
			% Want to have common siguature line with define_BOR_SURFACE.m to
			% retrieve the values that I previously used.
			% ref: define_BOR_Surface
			p=inputParser();
			p.addOptional('debugPlot',0)
			p.parse(varargin{:});
			debugPlot=p.Results.debugPlot;
			
			S=obj; 
			
			genNodeMP		= S.D3.genNode_MP_XYZ;
			thetas			= S.D3.thetas;
			phis			= S.D3.phis;
			genNodeTPMP		= S.D3.genNode_MP_TNP;
			PIM_BOR			= S.masterBOR.PIM_BOR;
			genNode_VTP_MP  = S.D3.genNode_VTP_MP;
			gen_rho			= S.D2.gen_rho;
			gen_z			= S.D2.gen_z;
			BOR_LCS_NF		= S.D3.genNode_MP_BorLCS;
			SPH_LCS_NF		= S.D3.genNode_MP_SphLCS;
			
			% Visualize PEC-BOR: What'd we get?
			if debugPlot==1 || debugPlot==2
				figure('position',[811         476        1214        1022]);
				hold on;
				plot3(gen_rho, zeros(size(gen_z)), gen_z, '-b.','displayname', 'Defined Points', 'handlevisibility', 'on');
				plot3(gen_rho(3:2:end-1), zeros(size(gen_z(3:2:end-1))) , gen_z(3:2:end-1),'-ro', 'markersize', 10, 'linewidth',2, 'displayname', 'Expansion Function locations', 'handlevisibility', 'on')
				%h=legend;
				
				%set(h,'location','best', 'visible', 'on');
				legend('show');
				legend('location', 'west');
				T=classTransforms;
				T.labelAxes('x', 'y', 'z', 'generatrix');
				axis equal; title('BOR');
				axis vis3d
				zoom(.8);
				%title('generatrix');
				
				drawnow
			end
		
			if debugPlot==2;
				%Plot the BOR in 3D?
				showBOR(genNodeMP);
				hold on;
				axis vis3d
				axis equal
			end
		end
		
		function show_LCS_error(obj)
			warning([mfilename ': only compares error to spherical GCS! THIMK!']);
			GCS=obj.D2.GCS;
			LCS=obj.D2.LCS;
			show_LCS_error(1:obj.NP, GCS, LCS);
		end
		
		function [gen_rho_ti, gen_z_ti, LCS_ti, GCS_ti, dx_ti, ddx_ti, sph_r_ti, sph_theta_ti]=interp1(obj, ti)
			% Given vector of (float) indices from childBor, PIECEWISE LINEAR interp lin between points.  
			% Contrast this with 'surface/smooth' method on generatrix
			% of masterBOR, method: "interps"
			% (class_child_BOR)
			generatrix=obj.D2.PIM_BOR.generatrix;
			gen_rho_ti=generatrix.cfit_rho(ti).';
			gen_z_ti=generatrix.cfit_z(ti).';
			dx_ti=generatrix.cfit_dx(ti).'; 
			ddx_ti=[0 diff(dx_ti)];
			sph_r_ti=generatrix.cfit_sph_r(ti).';
			sph_theta_ti=generatrix.cfit_sph_theta(ti).';
			
			% LCS/GCS *must* come from the masterBOR - needs the 'cts. curves'
			[LCS_ti, GCS_ti]=obj.masterBOR.get_GCS_LCS(ti);
		end
		
	end
	
end
