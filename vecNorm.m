function n = vecNorm(x, dim)
% VECNORM(x, dim)
% Norm of a vector
% If x is matrix, gets norm of every row or column

    if nargin < 2
        dim = 1;
    end

    n = sqrt(sum(conj(x).*x, dim));

	
	if max(imag(n))>eps
		warning([mfilename ': saw a slight imaginary number. probably numerica artifact']);
	end
	n=real(n);
	
%JOSH: Sept 22, 2019
% for complex-imaginary scalars, and not vector-components, can do
% CONJ=sqrt(ctranspose(E)*E);
	
return
	
% All of these ways to compute the field magnitude
% E* . E

NORME=norm(E);
disp(['Norm(E) = ' num2str(NORME)]);

% Conj(E)*conj(E)
CONJ=sqrt(ctranspose(E)*E);
disp(['CONJE(E)= ' num2str(CONJ)]);

%Sum of the squares
SOS= sqrt((conj(E(1))*E(1))+(conj(E(2))*E(2))+(conj(E(3))*E(3)));
disp(['SOS     = ' num2str(SOS) ]);

% 

% 2 more ways, all to say the same thing
%sqrt(sum(abs(E.^2)))
%sqrt(sum(abs(ctranspose(E)*E)))

VNORM=vecnorm(E);

% ALL EQUIVALENT FORMS
norm(E)
vecNorm(E)

sqrt(  sum( abs(E)  .^2)  )

sqrt(ctranspose(E)*E) 
sqrt(sum(E.*conj(E)))

sqrt(dot(E,E)) % nB; doc says dot(E,E) == conj(transpose(E))*E
sqrt(E'*E) 
sqrt(conj(transpose(E))*E)



