% Hi MASc Candidate! Hi Dr. McNamara!
%
% This codebase performs the integration, via discrete summations, of 
% weighted orientable electric and magnetic dipoles.  Dipoles use rigorous 
% complete vector forms in the near-field, and include
% 
% This work was originally developed by Josh Philipson the “Near- to Far-Field Transformation for Arbitrarily-Shaped Rotationally-Symmetric Antenna Measurement Surfaces” 
% •	Thesis document link: https://ruor.uottawa.ca/bitstream/10393/41434/5/Philipson_Joshua_Benjamin_Julius_2020_thesis.pdf 
% •	Thesis item base page https://ruor.uottawa.ca/handle/10393/41434 

% see the following examples codes for sample usage of the EMDSI calculator
%% VERSIONING
%             Author: Josh P.
%      Creation date: 27-Jan-2021 23:39:32
%             Matlab: 9.8(R2020a)
%  Required Products: 
% 
%% REVISIONS
% V1.0 | 2021-01-27 | Josh P.      | Creation
%

close all
clear all

EMDSI_TEST_1 % Single MDSI FF calculation (qty=1 magnetic dipole triplet, oriented to z-hat, output field in spherical basis [Er, Etheta,Ephi], where Er=0 in FF.
EMDSI_TEST_2 % Single MDSI FF, calculation (qty=1 magnetic dipole triplet, oriented to z-hat, output field in Cartesian basis [Ex,Ey,Ez]. Visualize on the principle planes.
EMDSI_TEST_3 % EMDSI NF calculation (qty=3 electric dipole triads + qty=3 magnetic dipoles triads, all at the origin, oriented along basis vectors)

% FF-versions are done identically to near field, except ARRAYP ought to be a 2xN array 
% of observation (FF) angles [theta;phi].', all in radians.
% In NF, ARRAYP are Cartesian triplets [x,y,z].'
%
%
% also, unrelated, note that []' takes CONJUGATE TRANSPOSE, 
% whereas [].' does just the TRANSPOSE.  
% I didn't know that after 15 years, and it was so hard to find/figure this out. 
% hth.  ;)  You owe me a beer! :0)

%
% Dr. McNamara requested
%
% Note: Thus the fact that the analytical expressions for the far-zone 
% fields of the infinitesimal dipole are known in its local coordinate 
% system is not something that ‘the user’ need worry about. %
% It would be nice if, in the MATLAB script, these appear explicitly
%
% response: See classElectricDipole.m and classMagneticDipole.m for the forms