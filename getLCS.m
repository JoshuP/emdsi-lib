function [n_hat_MP, t_hat_MP, phi_hat_MP, rho_hatMP, rho_MP, SPH_LCS]=getLCS(genNodeMP, genSpanMP, varargin)
%% Function to compute the array of Local Coordinate Systems
% MP indicates these are multiprod friendly arrays. (1 page per azimuth)
% Inputs are the Node array and Span array details.
% See notes for calculation detail. 
% Freely uses cross2 file, see; https://www.mathworks.com/matlabcentral/fileexchange/8782-vector-algebra-for-arrays-of-any-size--with-array-expansion-enabled

p=inputParser;
p.addOptional('skipTBarNormalizationCheck',0);
p.parse(varargin{:});

% We might do this if the t-vector becomes zero-vector 
% as it does for support BOR
skipTBarNormalizationCheck=p.Results.skipTBarNormalizationCheck;  


[~, N,M]=size(genNodeMP);

% JAN 1, 2020
% if M=1; then we are not using the _MP mode of this function
if M==1;
	mode=2;
else
	mode=1;
end

%%
global pp
%pp.TRACER.stack;


%% Compute phi_hat
phi=bsxfun(@atan2, genSpanMP(2,:,:), genSpanMP(1,:,:)); %Calculate phi 
														% = atan2(y,x)
x=-sin(phi);
y=cos(phi);
%z=zeros(1,(N-1),M); %ORIGINAL
z=zeros(size(phi)); %works for original (i.e. on SPANS) or concatenated 3xARRAYP array
phi_hat_MP=[x;y;z];

%% Compute rho_hat
mag=(1./sqrt(genSpanMP(1,:,:).^2+genSpanMP(2,:,:).^2)); % 1/sqrt(x.^2+y.^2)
if isinf(mag); %means radius==00 %Jan1,2020
	mag=0;
end
%mag(mag>1e10)=Inf;% Remove points that are really big
x=mag.*genSpanMP(1,:,:);
y=mag.*genSpanMP(2,:,:);
% Original
%z=mag.*zeros(1,(N-1),M);  % N-1, because there is N-1 spans, whereas  there are N nodes
z=mag.*zeros(size(mag));

rho_hatMP=[x;y;z]; 

%% check if rho_hatMP is already normalized
if 0
	A=(roundToPrecision(sqrt(rho_hatMP(1,:,:).^2+rho_hatMP(2,:,:).^2+ rho_hatMP(3,:,:).^2),-15));
	if mode==1;
		test=((N-1)*M);  %original; for mode=1;
	elseif mode==2;
		test=(size(phi,2));
	end
	%if round(sum(sum(A)))~=((N-1)*M);  %original; for mode=1;
	if round(sum(sum(A)))~=test && (mag~=0);   
		error('rho_hatMP is not normalized'); %and it will not be if mag==0;
	end

end
%% Compute t_hat
z_hatMP=[zeros(2,(N-1),M); ones(1,(N-1),M)]; % Compute z_hat
dz=diff(genNodeMP(3,:,:), [],2); %The Zn-Zn-1 diff in z-heights atnodes
dt=sqrt(sum(diff(genNodeMP, [],2).^2, 1));

%% Compute drho / dt  (RHO is the radial distance sqrt(x^2+y^2)
% The rho (in cylindrical coordinates) at the nodes
rho_MP=sqrt(sum(genNodeMP(1:2, :, :).^2,1));
% The difference in the RHO at the nodes, to be used at the span centers
drho=diff(rho_MP);							
%Create the Vector by piecewise multiplying
% the unit vectors by the correct scaling parameters.
t_bar_MP=bsxfun(@times, drho./dt, rho_hatMP) + ...
		 bsxfun(@times, dz./dt, z_hatMP); 

if ~skipTBarNormalizationCheck
	if round(  sum  (sum  (sum  (t_bar_MP.^2)  )  )  )   ~=((N-1)*M);
		error('t_bar_MP is not normalized'); 
	end
end

t_hat_MP=t_bar_MP;

%% Compute Normal
%n_hat=cross(phi_hat, t_hat);
%pagewise calculation of cross product to get the normal.s
n_hat_MP=cross2(phi_hat_MP, t_hat_MP); 
	
	
%% Dumb looper to get the SPHERICAL LCS at all points too 
%  (not just the LCS on the BOR COORDS)
% For one Azimuth at a time, do all points
% THIS PART USED FOR EXCITATION INTEGRAL CALCULATION of all points on
% the SPANS
T=classTransforms();
SPH_R_HAT=zeros(size(genSpanMP));
SPH_THETA_HAT=zeros(size(genSpanMP));
SPH_PHI_HAT=zeros(size(genSpanMP));

for iM=1:M
	P=genSpanMP(:,:,iM);
	
	[SPH_R_HAT(:,    :,iM), ...
	 SPH_THETA_HAT(:,:,iM), ...
	 SPH_PHI_HAT(:,:,iM) ]=T.LCS_SPHERICAL(P)	;
	
end
SPH_LCS.SPH_R_HAT=SPH_R_HAT;  %This exists at the SPAN points!
SPH_LCS.SPH_THETA_HAT=SPH_THETA_HAT;
SPH_LCS.SPH_PHI_HAT=SPH_PHI_HAT;

if 0
	% Jan1 2020; check agreement; for a sphere; better be bang on; GCS vs. LCS
	LCS.n_hat_MP	=	n_hat_MP(:,:,1);
	LCS.t_hat_MP	=	t_hat_MP(:,:,1);
	LCS.phi_hat_MP  =	phi_hat_MP(:,:,1);

	GCS.SPH_R_HAT=SPH_LCS.SPH_R_HAT(:,:,1);
	GCS.SPH_THETA_HAT=SPH_LCS.SPH_THETA_HAT(:,:,1);
	GCS.SPH_PHI_HAT=SPH_LCS.SPH_PHI_HAT(:,:,1);

	show_LCS_error(1:N-1, GCS, LCS);			%(xyz) values of GCS/LCS, & RelError for LCS v GCS
	error('quitting now');
end
