function PLANE_VECTOR_PLOT_DEMO(opt, field, Targs, nDim1, nDim2, CLOUD,PLANE)
T=classTransforms();
% %% DEFINE: "P OBSERVATION POSITIONS" (i.e. location on MH_BOR  where field is needed)
% [CLOUD, PLANE]=T.sliceView_getPointCloud(Targs{:});		
% ARRAYP=CLOUD.PtCloud;


% %% DEFINE: "P OBSERVATION POSITIONS" (i.e. location on MH_BOR  where field is needed)
% MDSI=EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, DIPOLES); 
% %% VISUALIZE FIELD INTENSITY
% field=MDSI.E_c_c;


magnitude=10.*log10(vecNorm(field));
if opt.normalize
	magnitude=magnitude-max(magnitude);
end

%Filter out data 3 stdev further than the mean
mu=mean(magnitude);
stdev=std(magnitude);
filter=mu-3*stdev;

magnitude(magnitude<filter)=filter;
if 0
	fprintf('[min, max] FIELD:[%.2f, %.2f]', min(vecNorm(field)), max(vecNorm(field)));
	fprintf('  MAGdB [min, max]:[%.2f, %.2f], [mean,std,filter]:[%.2f, %.2f, %.2f]', min(min(magnitude)), max(max(magnitude)), mu, stdev, filter);
end

% filter=find(magnitude<-100);
% magnitude(filter)=nan;

% magnitude=vecNorm(MDSI.E_c_c);
PLANE.MAG=PLANE.unflatten_(magnitude, nDim1, nDim2); %plot field strength fr
p=T.sliceView_Patch(PLANE); %Plot the color
set(p,'facealpha',1);
colorbar
colormap(jet(10000));
% axis(3.*[-1 1 -1 1 -1 1]);
% view(45,24);
xlabel('X-axis');ylabel('Y-axis');zlabel('Z-axis'); 
colormap(jet(1000));
% set(gcf,'renderer', 'opengl');

%% Plot 3d Vector quantities in the plane
x=PLANE.XX;
y=PLANE.YY;
z=PLANE.ZZ;
u=PLANE.unflatten_((field(1,:)), nDim1, nDim2);
v=PLANE.unflatten_((field(2,:)), nDim1, nDim2);
w=PLANE.unflatten_((field(3,:)), nDim1, nDim2);
if opt.DoQuiver
	scale=opt.QuiverScaleFactor;
	quiver3(x,y,z, u, v, w, scale, 'linewidth',1, 'color', 'b');
end
 