% EMDSI_TEST_4

% Pertaining to email sent: On Wed., Mar. 31, 2021, 7:58 a.m. Eqab Rateb AL Majali, <ealmajali@sharjah.ac.ae> wrote:
% 		Dear Josh,
% 		Hope you are doing well.
% 		Attached files contain the total (incident + scattered)  electric and magnetic near-zone fields predicted on a surface of a sphere of radius r= 0.63m for a feed-horn antenna I've modeled, using FEKO, at a centre frequency of 2.08 GHz . The data is taken from FEKO output file of the said model in Global Spherical Coordinate.  I left the table header , as taken from the output file, to indicate coordinates and data arrangement. The # of theta samples =91 hence delta_theta=2 degrees while the # of phi samples =181 (it is oversampled intentionally to increase the calculation accuracy using the infinitesimal dipoles).  
% 		Please kindly use your code to find out the equivalent dipoles and the far-zone fields for this model. Once you get the far-field data, we will compare these results to the ones calculated using FEKO . Once your code is adapted to work out the calculation of this example, we can perhaps meet to talk about the changes you made such that I can use for the many examples to come.
% 		Thanks for your help and have a wonderful day.
% 		Regards, Eqab

%
% JOSH's Comments in email sent: On Thursday April 15, 2021, 12:08am,
% philipson00@gmail.com wrote:
% 		Dear Eqab,
% 		I hope you are well.
% 		
% 		Status
% 		I have implemented what you have requested in "EMDSI_TEST_4.m".  
% 		Please pull the latest revisions from the EMDSI-lib repository here.
% 
% 		Detail
% 		I generated far-zone fields in the same observation directions as the THETA,PHI set as the measurement locations.I have generated output in EMDSI_TEST_4 RESULT, outputted as a .txt and .mat.  This is in the repo and attached here.
% 		In previous discussions I did not mention scaling results by the 'differential geometry' to account for measurement/frustum density. Derek can explain, or you can see the thesis, in the cited parts (please read comments in "EMDSI_TEST_4.m".
% 		Next Steps
% 		Please let me know if you have questions or further requests...and I will be SHOCKED if we get matching results right out of the gate - so please let me know.What I did last time when I was lost...was do a single dipole at the origin, and then build up complexity to debug the calculation chain.  We can do the same if you generate measurements for such a case.  Just let me know what you're thinking.
			
%% ENVIRONMENT
close all;
clear all;
T=classTransforms();
RAD2DEG = 180/pi;
DEG2RAD = pi/180;

%% READ E_field & H_field each in turn. Note conversions

% NOTE: I DISCARDED THE LONGITUDE DATASET FOR PHI=360, as it's duplicate
% for PHI=0

fprintf('  loading files..\n')
[E_FILE]=load_file_v1('2021-03-31-Total-E-Field.txt');
[H_FILE]=load_file_v1('2021-03-31-Total-H-Field.txt');
fprintf('done\n')

% Check to see that the measurement locations are the same in both files
assert(max(vecNorm(E_FILE.ARRAYF_2D_XYZ-H_FILE.ARRAYF_2D_XYZ)) ==0) % Take difference between both sets of Cartesian locations. Better be zero. Zero=same.

%% Options
frequency = 2.08e9; % Hz
c = 2.99792458e8; % m/s
lambda = c / frequency;
k0 = 2*pi / lambda; %Wavenumber for dipoles
npts = size(E_FILE.FIELD_c_c, 2) ; % # of field measurements

%% Geometry information from the file
uThetas = unique(E_FILE.ARRAYF_2D_TNP(2,:)); % unique thetas
uPhis=unique(E_FILE.ARRAYF_2D_TNP(3,:));

nthetas = numel(uThetas); % # of unique thetas
NP=nthetas; % points along generator
nphis = numel(uPhis); % # unique phis
M=nphis; % number of unique phi values (no overlap on wrap)

deltaTheta = uThetas(2)-uThetas(1); % radians
deltaPhi = uPhis(2)-uPhis(1); 

%% Dipole {c'} locations
ARRAYF = E_FILE.ARRAYF_2D_XYZ; % 3x npts array.  Each measurement point on Eqab's sphere is a measurement. There are 16471 such points.


%% Magnitudes
electric_dipole_magnitude = 1;
magnetic_dipole_magnitude = 1;


%% orientation of the diplole triad(s) {c'}, expressed in global-basis
x_hat = repmat([1;0;0],1,npts); % 3x npts arrays shown for npts dipole. 
y_hat = repmat([0;1;0],1,npts);
z_hat = repmat([0;0;1],1,npts);


%% Differential Area
%
% REF: Josh Philipson MASC Thesis Submission.pdf
% Equation (4.4-7), (4.4-8), the dA term
% "The factor dA, shown in Section 4.4.2 is related to the differential
% geometry of the frustum represnetation of the BOR, and accounts for the
% elemental surface area that appears in (4.4-1) and (4.4-2).  By accounting
% for the measurement density and surface geometry in this way, non-uniform
% (spatial) sets of sampling densities may be 'corrected' and be made
% compatible.
%
% further; An analogy:
%  Measurement density is a bias in our spherical representation.  
%  Imagine we have 1,000,000 points in a local region - we don't want this
%  surface sampling density to be akin to a much higher power - rather it
%  ought to give a very precise estimation of the value. We thus must scale
%  measurements by the size of the area it represents.
%  i.e. a 'lightbulb' to be burning so bright it outshines the rest of the system
%  - in fact we want to prorate each measurement by 1/1,000,000 to give a
%  good average, taken in whole.

% see thesis for better explanation - short version; we scale dipole
% strengths by representative differential area of each element

% NOTE:**** 
% There is an exploitation of the organization in the datafile
% In order to compute the frustum differential area, I must assume that
% data is organized along lines of longitude from theta=0 to
% theta=180degrees, for lines of constant phi.  If this changes, then we'll
% have to revisit this calculation


% Sanity checks in data structure (Look at positions in spherical coords)
assert(all(E_FILE.ARRAYF_2D_TNP(2,1:nthetas)==E_FILE.ARRAYF_2D_TNP(2,nthetas+1:nthetas+nthetas))); %This checks 2 different phi-values of data must have same theta-values
assert(~any(E_FILE.ARRAYF_2D_TNP(3,1:nthetas)==E_FILE.ARRAYF_2D_TNP(3,nthetas+1:nthetas+nthetas))); % Check phi are all different for each theta-value


% get rho/phi, for generator curve at phi=0, the rho value is same as
% x-value. (Look at positions in cartesian coords)
gen_rho = E_FILE.ARRAYF_2D_XYZ(1, 1:nthetas);
gen_z = E_FILE.ARRAYF_2D_XYZ(3, 1:nthetas);



%% have gen_rho, gen_z, let's invoke the BOR class methods - to find LCS,GCS

% SUBSAMPLE: DEBUG (to visualize GCS)
subsample = 0;
if subsample
	gen_rho=gen_rho(1:9:end);
	gen_z = gen_z(1:9:end);
	M=4;
end


NP=numel(gen_z); %ta
master_BOR = class_master_BOR('EQAB', NP, [], 'gen_rho_0', gen_rho, 'gen_z_0', gen_z);
master_BOR.name = 'EQAB_MASTER';
[LCS_GEN, GCS_GEN]=master_BOR.get_GCS_LCS([1:NP]); % GETS GCS,LCS on generator

% BOR_CHILD method
S=class_child_BOR(master_BOR, NP, M);% CREATE BOR of Revolution based on generatrix
S.name='EQAB';													%NAME IT

[genNodeMP_NFMBOR, thetas, phis, genNodeTPMP_NFMBOR, PIM_BOR, ...
	genNode_MP_VTP_NFMBOR, ~, ~, BOR_LCS, SPH_LCS]=S.interface('debugPlot', 0);

if subsample % optional visualize of the LCS/GCS
	GCS=SPH_LCS;
	genNodeMP = S.D3.genNode_MP_XYZ;
	showBOR(genNodeMP)
	ARRAYP=reshape_flatten_MP(genNodeMP, 3, NP*M, 1);
	%show_GCS_or_LCS(ARRAYP, LCS, 'interval', 1);  % LCS vectors in 3D
	show_GCS_or_LCS(ARRAYP, GCS, 'interval', 1);  % GCS vectors in 3D
	stop
end


%% continue
dAi=computeDifferentialArea(gen_rho, gen_z); %Frustum Areas from most negative z to largest +z
TotalArea=sum(dAi);   % May 22, 2019, for sphere; AREA=4*pi*r^2
dA=(deltaPhi ./ (2*pi)) * dAi; % The area per differential volume. (each latitude has a bounding area)

% dA above is for one 'generator curve at constant phi'. Reproduce this so
% I can process points from 1:npts (i.e. series of 'lines of constant
% longitude

dA_ = repmat(dA, 1, nphis);

% sanity check the differential area array is 1-for-1 same size as the
% dipole magnitudes we have to scale (by dA).
assert(size(dA_,2) == npts)

%% NOTE ON EDSI vs MDSI
% MDSI is Magnetic Dipole Superposition Integral, and hence uses Magnetic Dipoles as radiating elements
%		In this case, the dipole weights come from the E-field measurements
%		M =  Mx (^x)  +  My (^y)  +  Mz (^z)
%		M =  Ex (^x)  +  Ey (^y)  +  Ez (^z)
%
% previous------------------------
%	M_NF =     Mt  (^t) + Mphi (^phi)
%	M_NF =   (-E_phi) (^t) + (+E_t) (^phi) )  ; %Corrected May 15/2019
%
%			therefore
%					Mt = -E_phi
%				 M-phi = +E_t

% EDSI is Electric Dipole Superposition Integral, and hence uses Electric Dipoles as radiating elements
%		In this case, the dipole weights come from the E-field measurements
%		J =  Jx (^x)  +  Jy (^y)  +  Jz (^z)
%		J =  Hx (^x)  +  Hy (^y)  +  Hz (^z)
% previous ------------------------
%	J_NF =     Jt  (^t) + Jphi (^phi)
%	J_NF =   (H_phi) (^t) + (-H_t) (^phi) )  ; %Corrected May 15/2019
%
%			therefore
%					Jt = H_phi
%				 J-phi = -H_t
%

E_tot=smd(NHEC).KT(KT).FIELD_c;
E_theta=dot(SPH_GCS.SPH_THETA_HAT, E_tot); % dot (LCS|GCS, field)!! this is the way! See CALCULATE_INTEGRALS_FFT_v2
E_phi=dot(SPH_GCS.SPH_PHI_HAT,E_tot);
field_dot_BOR=dot(bor_lcs, reshape (  FIELD_MP(:,isp, [1:NAZ]), 3, NAZ)); %Take field component and dot it with BOR_LCS

%% Electric dipole triads at each measurement point
fprintf(['  creating ' num2str(npts) ' electric dipoles..']);
for i = 1:npts
	% X-component
	Ex = E_FILE.FIELD_c_c(1,i); %Complex representation of E_x, to set dipole px magnitude.
	[I_0, PHASE] =  cart2pol(real(Ex), imag(Ex));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	E_DIPOLES.x_dipoles(i) = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
	
	% Y-component
	Ey = E_FILE.FIELD_c_c(2,i); %Complex representation of E_y, to set dipole py magnitude.
	[I_0, PHASE] =  cart2pol(real(Ey), imag(Ey));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	E_DIPOLES.y_dipoles(i) = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
	
	
	% Z-component
	Ez = E_FILE.FIELD_c_c(3,i); %Complex representation of E_z, to set dipole pz magnitude.
	[I_0, PHASE] =  cart2pol(real(Ez), imag(Ez));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	E_DIPOLES.z_dipoles(i) = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
end
fprintf('done!\n');


%% Magnetic dipoles
fprintf(['  creating ' num2str(npts) ' magnetic dipoles..']);

for i = 1:npts
	% X-component
	Hx = H_FILE.FIELD_c_c(1,i); %Complex representation of H_x, to set dipole px magnitude.
	[I_0, PHASE] =  cart2pol(real(Hx), imag(Hx));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	M_DIPOLES.x_dipoles(i) = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Magnetic Dipole Class
	
	% Y-component
	Hy = H_FILE.FIELD_c_c(2,i); %Complex representation of H_y, to set dipole py magnitude.
	[I_0, PHASE] =  cart2pol(real(Hy), imag(Hy));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	M_DIPOLES.y_dipoles(i) = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Magnetic Dipole Class
	
	
	% Z-component
	Hz = H_FILE.FIELD_c_c(3,i); %Complex representation of H_z, to set dipole pz magnitude.
	[I_0, PHASE] =  cart2pol(real(Hz), imag(Hz));
	I_0 = I_0 * dA_(i); % Scale by differential Area
	FF_Approximation = 1; % far-field form of dipole
	M_DIPOLES.z_dipoles(i) = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Magnetic Dipole Class
end
fprintf('done!\n');

%% "ARRAYP" Observation Points (i.e. location where field is needed)

% I CHOSE POINTING DIRECTIONS IN THE FF in the same (THETA,PHI) set as the
% original dipoles.
%
% you could just have easily created a different array for ARRAYP (2xN for
% "N" observation directions), corresponding to [THETA;PHI] pointing angles
% (in radians)

% We'll take the same pointing (theta;phi) angles as the original dipole
% locations (though they were at r=0.63m), now our pointing directions are
% in those same angles, (imagine rays from origin, passing thru measurement
% points out towards infinity).


ARRAYP = E_FILE.ARRAYF_2D_TNP(2:3,:); % Omit row 1('r'), and only take 'theta; phi], rows 2:3, all columns, so we'll have npts observation directions
disp(['  setting ' num2str(size(ARRAYP, 2)) ' observation directions..done!']);

%% Compute EMDSI superposition at all my observation points.
fprintf(['*********  Starting EMDSI Calculations  *********\n']);
EDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, E_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one
MDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, M_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one

% ELECTRIC FIELD
E_EMDSI_FIELD_c = EDSI.E_c_c + MDSI.E_c_c; % Cartesian
E_EMDSI_FIELD_s = EDSI.E_c_s + MDSI.E_c_s; % Spherical
% MAGNETIC FIELD
H_EMDSI_FIELD_c = EDSI.H_c_c + MDSI.H_c_c; % Cartesian
H_EMDSI_FIELD_s = EDSI.H_c_s + MDSI.H_c_s; % Spherical


% Field is represented in global basis [Ex; Ey; Ez] on a cartesian basis

%% Save Data file, preferably in same format (or close) to Eqab.

%Make table to get nice dataformat, like Eqab's .txt
% let's convert my complex-field format for Er, Etheta, Ephi to magnitude
% and phase
THETA = ARRAYP(1,:)*RAD2DEG;
PHI = ARRAYP(2,:)*RAD2DEG;

% E-field, nb; ER=0 in FF, so omit.
E_THETA = E_EMDSI_FIELD_s(2,:);
E_THETA_MAG = vecNorm(E_THETA );
E_THETA_PHASE = angle(E_THETA) ;

E_PHI = E_EMDSI_FIELD_s(3,:);
E_PHI_MAG = vecNorm(E_PHI);
E_PHI_PHASE = angle(E_PHI);

% H-field, nb; HR=0 in FF, so omit.
H_THETA = H_EMDSI_FIELD_s(2,:);
H_THETA_MAG = vecNorm(H_THETA );
H_THETA_PHASE = angle(H_THETA) ;

H_PHI = H_EMDSI_FIELD_s(3,:);
H_PHI_MAG = vecNorm(H_PHI);
H_PHI_PHASE = angle(H_PHI);

t=table(THETA.', PHI.',...
	E_THETA_MAG.', E_THETA_PHASE.', E_PHI_MAG.', E_PHI_PHASE.',...
	H_THETA_MAG.', H_THETA_PHASE.', H_PHI_MAG.', H_PHI_PHASE.', ...
	'VariableNames', {'THETA_deg', 'PHI_deg', ...
	'E_THETA_MAG', 'E_THETA_PHASE_rad', 'E_PHI_MAG', 'E_PHI_PHASE_rad',...
	'H_THETA_MAG', 'H_THETA_PHASE_rad', 'H_PHI_MAG', 'H_PHI_PHASE_rad',...
	});

% Generates
%     THETA_deg    PHI_deg    E_THETA_MAG    E_THETA_PHASE_rad    E_PHI_MAG     E_PHI_PHASE_rad    H_THETA_MAG    H_THETA_PHASE_rad    H_PHI_MAG    H_PHI_PHASE_rad
%     _________    _______    ___________    _________________    __________    _______________    ___________    _________________    _________    _______________
%         0           0       4.4876e+05           1.9056         1.0647e+06          1.481          2826.3            -1.6606          1191.2           1.9056    
%         2           0        4.714e+05           1.2816         9.5439e+05         1.0808          2533.4            -2.0608          1251.3           1.2816    
%         4           0       5.1186e+05          0.90184         1.1124e+06        0.77573          2952.8            -2.3659          1358.7          0.90184    
%         6           0       4.5077e+05          0.71922         1.2276e+06        0.72712          3258.5            -2.4145          1196.5          0.71922  

% Dump sample of the table?
if 1
	disp(['Sample output file format dumped to console below' char(10)]);
	disp(head(t))
end

% Write to file
fprintf(['  writing output files..']);
save('EMDSI_TEST_4 RESULT.mat', 't'); % Binary save
writetable(t,'EMDSI_TEST_4 RESULT.txt','Delimiter',',');  % Text save
fprintf('!\n');
disp('  done & sunny!');

