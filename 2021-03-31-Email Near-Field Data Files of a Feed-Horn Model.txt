 Dear Josh,

Hope you are doing well.

Attached files contain the total (incident + scattered)  electric and magnetic near-zone fields predicted on a surface of a sphere of radius r= 0.63m for a feed-horn antenna I've modeled, using FEKO, at a centre frequency of 2.08 GHz . The data is taken from FEKO output file of the said model in Global Spherical Coordinate.  I left the table header , as taken from the output file, to indicate coordinates and data arrangement. The # of theta samples =91 hence delta_theta=2 degrees while the # of phi samples =181 (it is oversampled intentionally to increase the calculation accuracy using the infinitesimal dipoles).  

Please kindly use your code to find out the equivalent dipoles and the far-zone fields for this model. Once you get the far-field data, we will compare these results to the ones calculated using FEKO . Once your code is adapted to work out the calculation of this example, we can perhaps meet to talk about the changes you made such that I can use for the many examples to come.

Thanks for your help and have a wonderful day.

Regards,
Eqab


FILE 1: Total-E-Field.txt renamed to 2021-03-31-Total-E-Field.txt
FILE 2: Total-H-Field.txt renamed to 2021-03-31-Total-H-Field.txt
