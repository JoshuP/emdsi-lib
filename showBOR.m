function figureHandle=showBOR(MP_STRUCT, varargin)
% function figureHandle=showBOR(MP_STRUCT, varargin)
%
% Optional('figureHandle', []);

p=inputParser;
p.addOptional('figureHandle', []);
p.addOptional('color', []);
p.addOptional('marker', '.');
p.addOptional('markersize', 6);
p.addOptional('linestyle', '-');
p.addOptional('markerfacecolor','black');
p.addOptional('MarkerEdgeColor','black');
p.addOptional('titleString', '3D BOR');
p.addOptional('linewidth', 1);

p.parse(varargin{:});
figureHandle=p.Results.figureHandle;
userColor=p.Results.color;
marker=p.Results.marker;
markersize=p.Results.markersize;
linestyle = p.Results.linestyle;
titleString = p.Results.titleString;
markerfacecolor = p.Results.markerfacecolor;
MarkerEdgeColor  = p.Results.MarkerEdgeColor ;
linewidth= p.Results.linewidth ;

if isempty(figureHandle)
	figureHandle=figure('position',[811         476        1214        1022]);
	grid on; 
	hold on;
else
	try
		figure(figureHandle);
	catch
	end
	hold on;
end

[~,NP, M]=size(MP_STRUCT);

for i=1:M;
	if isempty(userColor)
		pcolor=[rand rand rand];
	else
		pcolor=userColor;
	end
	%plot3(MP_STRUCT(1,:,i), MP_STRUCT(2,:,i), MP_STRUCT(3,:,i), '-', 'color', 'blue', 'marker', marker, 'markersize', markersize);
	plot3(MP_STRUCT(1,:,i), MP_STRUCT(2,:,i), MP_STRUCT(3,:,i), 'linestyle', linestyle, 'color', pcolor , 'marker', marker, 'markersize', markersize, 'markerfacecolor', markerfacecolor, 'MarkerEdgeColor', MarkerEdgeColor , 'linewidth', linewidth);
	
end

zoom(0.7);
xlabel('x-axis');
ylabel('y-axis');
zlabel('z-axis');
view(117,32);
title(titleString );
axis equal vis3d