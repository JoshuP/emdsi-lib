function [H_c_s, E_c_s, TX_C2S_C, H_c_c, E_c_c]=RAHMAT_SAMII_FF(alpha, beta, gamma, P, f, varargin)

%% REVISIONS
% V1.0 | 2021-01-24 | Josh P.      | Creation, derive from: getFieldSubComponent_FF_V1.m
%
%Ep_cp_s=[E_r; E_theta; E_phi];

% FAR-FIELD VERSIOn!
% Assume (alpha, beta, gamma) are the Euler angles that rotate x-hat,
% y-hat,z-hat to be coincident upon {c'} in the special case that
% rp==0, so f=0.


% ARRAY OF POINTS P, (PAGE) VECTORIZED
%
% Given a feed at f, and a point P (where I wish to know the field
% components), and a LCS relationship via [alpha beta gamma], compute
% the E and H in spherical components.
% REF: "UsefulCoordinate Transformations for Antenna Applications
%		Y.RAHMAT-SAMII,
%		IEEE trans. on Ant & Prop, Vol. AP-27, No.4 July 1979
%
% On Notation:
%			 r_c_c		- denotes vector r  in {c}, in cartesian coordinates
%			 rp_c_c		- denotes vector r' in {c}, in cartesian coordinates
%			 rp_cp_c	- denotes vector r' in {c'},in cartesian coordinates
%			 rp_cp_s	- denotes vector r' in {c'},in spherical coordinates (not used in practice for r')
%
% Step 1: 

%% Input Parser
p=inputParser();
p.addOptional('TX_C2S_C', []);
p.addOptional('Dipole', 0);
%p.addOptional('EMDSI_SELECTOR', 'MDSI'); %By default compute MDSI
p.parse(varargin{:});
TX_C2S_C=p.Results.TX_C2S_C;
%EMDSI_SELECTOR=p.Results.EMDSI_SELECTOR;
M=p.Results.Dipole; 
T=classTransforms;	


%% Constants
RAD2DEG=180/pi;
DEG2RAD=pi/180;
degC=char(176);
fstr='%.2f';

%% Debug measures
if ~isAValidDipole( M) % Create dummy one if this is not passed in.
	lambda=2e-1; %0.02=2cm
	k_0=2*pi/lambda;
	I_0=1e0;
	M=classMagneticDipole(I_0,k_0); % Magnetic Dipole Class
	M.FF_Approximation=1;
	error('have not added EDSI case here...have hardcoded classMagneticDipole - do we need to add electric equivalent?')
else
	
end

% Have a Test Rotation, via c_cPrime_Tool
%angle=10; alpha=0*DEG2RAD; beta=0*DEG2RAD; gamma=angle*DEG2RAD;  %ROTATION ABOUT Z

%% Summary
% STEP 1 - Get BORSHAPE phase equivalent offsets equivalent phase offsets for this position 
% STEP 2 - Calculate transform CP2C for this feed location
% STEP 3 - Compute TX_C2S_C (one time, then 
% STEP 4 - Compute TX_S2C_cp (more complex)
% STEP 5 - Translate direction P to direction P' in {c'}
% STEP 6 - Calculate field in spherical coordinates {c'}

%% STEP 1 - BORSHAPE phase offsets
% Wave Number
k_0=M.k_0;
numPts=size(P,2);

% Observation Direction "P"
THETAS=P(1,:); % All far-field theta values for pt. P.
PHIS=P(2,:);   % same as above, but phi-values


% 'f' emitter location in {c}
x0=f(1);
y0=f(2);
z0=f(3);

% Calculation fo BOR_PHASE_OFFSETS for this feed location, for all observation directions
BOR_PHASE_OFFSETS=exp(1j.*k_0.* (x0*sin(THETAS).*cos(PHIS) + y0.*sin(THETAS).*sin(PHIS) + z0.*cos(THETAS)    )   )  ;
%warning([mfilename ': inserted a - sign']);

%% STEP 2: Compute [CP2C] & [C2CP]
CP2C=T.TX_CP2C(alpha, beta, gamma); %Update Euler Transform according to the true Euler angles
C2CP=T.TX_C2CP(alpha, beta, gamma);

%% STEP 3: CALCULATE [TX_C2S_C] - easy- for the THETA,PHI set we have
% Assemble 3x3 transform arrays, on on each page
% Pages correspond to each direction P we are examining

	if isempty(TX_C2S_C) % means it is the first time this subroutine is called. We need to calculate TX_C2S_C (Once..then it is passed back into this sub, on subsequent calls	
		TX_C2S_C=arrayfun(@T.TX_C2S, THETAS(:), PHIS(:), 'UniformOutput', 0);%Calculate all 3x3 transformation matrices all at once
		TX_C2S_C=reshape(cell2mat(TX_C2S_C.'),3,3,numPts); % Get each rotation matrix in its own page. 3x3xN
	end
	% NB: Critical, the THETAS(:) and PHIS(:), changes dimensions to
	% [numpts,1] from [1,numpts] in the case of THETAS() and PHIS()
	% array is flipped and is wrong in the reshape
	
%% STEP 4: Translate direction P(theta,phi) to direction P'(theta', phi') in {c'}
% Compute each part, then assemble on a 'paged' point of view to use
% multiPlane maths

	% Use equation (j1) to solve for [b1;b2;b3]
	R_C=[sin(THETAS).*cos(PHIS); sin(THETAS).*sin(PHIS); cos(THETAS)];
	R_CP=C2CP*R_C; %This is the [b1,b2,b3].' array where each column is for each direction P but in {c'};

	b1=R_CP(1,:); % =s(t') c(t')
	b2=R_CP(2,:); % =s(t') s(p');
	b3=R_CP(3,:); % =c(t')

	%Convert the theta-prime and phi-prime.
	% The equations we developed work for all angles of theta-prime except
	% theta-prime=[0,180]. However at these values of theta-prime the vlue
	% of phi-prime never matters, we can set it to any value we wish.
	
	THETA_PRIME=acos(b3);				% from A31
	badIdx=find(THETA_PRIME==0 | THETA_PRIME==180.*T.DEG2RAD);
	PHI_PRIME=acos(b1 ./ sqrt(1-b3.^2));% from A23
	
	eps=1e-5; %ORIGINAL
	
	
	
	% Numeric artifacts - small imaginary residual
	qty1=max(abs(imag(THETA_PRIME)));
	if qty1>=eps
		warn_str=['Possible numeric issue in ' mfilename '.  Maximum IMAG(theta)=' num2str(qty1)];
		warn(warn_str);
	end
	qty2=max(abs(imag(PHI_PRIME)));
	if qty1>=eps
		warn_str=['Possible numeric issue in ' mfilename '.  Maximum IMAG(theta)=' num2str(qty1)];
		warn(warn_str);
	end
	THETA_PRIME=real(THETA_PRIME);
	PHI_PRIME=real(PHI_PRIME);
	
	
	%Replace the bad values with so
	
%% STEP 5: Compute TX_S2C_cp (more complex)	(NOTE Herei've transposed the eq(4) in RAHM!!)
% so it truly is the Spherical to cartesian . this confusedd
	A11=b1;
	A21=b2;
	A31=b3;
	A32=-sqrt(1-b3.^2);
	A33=zeros(1,numPts);
	A23=b1./sqrt(1-b3.^2);
	A13=-1.*b2./sqrt(1-b3.^2);
	A12=b3.*b1./sqrt(1-b3.^2);
	A22=b3.*b2./sqrt(1-b3.^2);

	%Replace bad values in here with the zero we ought to get if theta'=0 | 180
	A12(badIdx)=0;
	A13(badIdx)=0;
	A22(badIdx)=0;
	A23(badIdx)=0;
	
	
	%ASSEMBLE array, PAGED so the transform is on different direction pt.P
	TX_S2C_CP=zeros(3,3,numPts);
	%for i=1:numPts
	%	TX_S2C_CP(:,:,i)=[A11(i), A12(i), A13(i);...	
	%					  A21(i), A22(i), A23(i);...	
	%					  A31(i), A32(i), A33(i)];
	%end
	
	% CORRECT VERSION, Verified June 15 2020.  
	
	TX_S2C_CP(1,1,:)=reshape(A11,1,1,numPts);
	TX_S2C_CP(1,2,:)=reshape(A12,1,1,numPts);
	TX_S2C_CP(1,3,:)=reshape(A13,1,1,numPts);
	
	TX_S2C_CP(2,1,:)=reshape(A21,1,1,numPts);
	TX_S2C_CP(2,2,:)=reshape(A22,1,1,numPts);
	TX_S2C_CP(2,3,:)=reshape(A23,1,1,numPts);
	
	TX_S2C_CP(3,1,:)=reshape(A31,1,1,numPts);
	TX_S2C_CP(3,2,:)=reshape(A32,1,1,numPts);
	TX_S2C_CP(3,3,:)=reshape(A33,1,1,numPts);
	% NOTE: TX_S2C_CP blows up at locations where theta_prime is [0,180]

	
	
%% STEP 6: Calculate Spherical Vector Quantities in {c'};
% rp_cp_s is the spherical coords of P in {c'}..
	[H_r, H_theta, H_phi, E_r, E_theta, E_phi]=M.FieldInSph(nan(1,numPts), THETA_PRIME, PHI_PRIME);
	Hp_cp_s=[H_r; H_theta; H_phi]; %Assemble vectors
	Ep_cp_s=[E_r; E_theta; E_phi];
	
	%Apply phase change
	DELTA=repmat(BOR_PHASE_OFFSETS,3,1); %multiply each of [Er,Etheta,Ephi] and [Hr, Htheta,Hphi)
	Hp_cp_s=bsxfun(@times, Hp_cp_s,DELTA ); 
	Ep_cp_s=bsxfun(@times, Ep_cp_s, DELTA); % multiply 
	
	
%% STEP 8: All Conversions at once to get Cartesian & Spherical components in {c}
%	H_c_s=TX_C2S_C*CP2C*TX_S2C_CP*Hp_cp_s; % Logic of transforms
%	E_c_s=TX_C2S_C*CP2C*TX_S2C_CP*Ep_cp_s;
% Page Vectorized Calculation

	TRANSFORM1=multiprod(CP2C, TX_S2C_CP); %Tranform to get back cartesian elements in {c}
	TRANSFORM2=multiprod(TX_C2S_C, TRANSFORM1); %Last additional transform to get back to Spherical in {c}
	
	
	L=multiprod(TRANSFORM1, reshape(Hp_cp_s,3,1,numPts)); 
	H_c_c=reshape(L, 3,numPts);	
	L=multiprod(TRANSFORM2, reshape(Hp_cp_s,3,1,numPts));
	H_c_s=reshape(L, 3,numPts);
	
	
	L=multiprod(TRANSFORM1, reshape(Ep_cp_s,3,1,numPts));
	E_c_c=reshape(L, 3,numPts);	% imagine z-directed dipole. In natural system has only phi- components. Expressing in cartesian, means no Z!
	
	L=multiprod(TRANSFORM2, reshape(Ep_cp_s,3,1,numPts));
	E_c_s=reshape(L, 3,numPts);
	
	
	if sum(any(isnan(H_c_s))) || sum(any(isnan(E_c_s))) 
		disp([mfilename ' just popped you up to keyboard!']);
		keyboard;
    end
	
%% STEP 9: FAR-FIELD CORRECTIONS: JAN 12 2020
% warning([mfilename ': FF correction - seems that I only have spherical quantities in the far field - eliminating the cartesian']);
% 
% % I think I swapped things to make it similar to the Near-Field case.
% % To correct this, I will 
% E_c_s=E_c_c; % [Er, Etheta, E_phi
% E_c_c=zeros(size(E_c_s));
% H_c_s=H_c_c;
% H_c_c=zeros(size(E_c_s));
	