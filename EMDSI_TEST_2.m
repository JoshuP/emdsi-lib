%% VERSIONING
%             Author: Josh P.
%      Creation date: 24-Jan-2021 19:18:58
%             Matlab: 9.8(R2020a)
%  Required Products: 
% 
%% REVISIONS
% V1.0 | 2021-01-24 | Josh P.      | Creation, derive from MDSI_Sanity_DIPOLE_MAGNETIC_10c 
%

% Derive from
%		COMPUTE_INCIDENT_FIELDS_NF_V1; %prepare for MDSI
%		getElementContributions_V2 % MDSI
%		getFieldSubComponent_V2

% GOAL: Create a single Magnetic dipole at the origin, z-aligned and create a plot
% given: p= px (x_hat) + py (y_hat) + pz (z_hat), only pz ~= 0


% close all
clear all
%%ENVIRONMENT
db_stash();
T=classTransforms();

%Monitor 1
fig1=figure('position',[569         547        1296         951]);
set(fig1, 'color','white')

%Maximize?
%pause(0.001); frame_h = get(handle(gcf),'JavaFrame');  set(frame_h,'Maximized',1);

[X_c, Y_c, Z_c]=T.getBasisVector();	%Basis vectors in {c}
T.labelAxes('x', 'y', 'z', '', 'fontsize', 26);
simName=mfilename;
text(0,3.5,3.5, ['INTENSITY PLOT: ' simName ' '], 'horizontalalignment', 'center','fontsize', 20, 'color',rgb('neon green'), 'interpreter','none', 'backgroundcolor', rgb('dark teal'), 'edgecolor',rgb('neon green'))
T.drawABN(     X_c,     Y_c,     Z_c ,'figurehandle', fig1, 'linewidth',1, 'linestyle', '-');	% {c}
[details]=DrawPlanes_v1('plotPlanes',1,'ScaleFactor', 3, 'BlankFaces',1);

hold on;
if 0
	setView('XZ');
	setView('YZ');
	setView('XY');
end

%% OPTIONS
opt.DoQuiver=0;
opt.QuiverScaleFactor=1;
% Sampling Density
nDim1=70;
nDim2=70;
k=50; %Wavenumber for magnetic dipole

%% DEFINE: Dipole Locations 
ARRAYF=[0 ;0; 0]; 
% ARRAYF=[ARRAYF,  [-2;0;-2 ]]
N=size(ARRAYF,2);
%  plot3(ARRAYF(1,:), ARRAYF(2,:), ARRAYF(3,:), '*', 'markersize', 10, 'markerfacecolor',rgb('yellow green'), 'linewidth', 2, 'markeredgecolor', rgb('azure'), 'linewidth',5)
hold on;

%% DEFINE: orientation of the diplole triad(s) {c'}, expressed in global-basis
% % BOR_LCS=[0;0;1]; %Along +z direction (natural emitter basis co-incident)
y_hat=[1;0;0];
x_hat=[0;1;0];
z_hat=[0;0;1];

%Replicate orientation arrays
y_hat=repmat(y_hat,1,N);
x_hat=repmat(x_hat,1,N);
z_hat=repmat(z_hat,1,N);

% Play with this if you want to reorient the single dipole
M=T.RotZ(T.DEG2RAD*0)*T.RotX(T.DEG2RAD*0)*T.RotY(T.DEG2RAD*0);
y_hat=M*y_hat;
x_hat=M*x_hat;
z_hat=M*z_hat;


%% DEFINE: Dipole Magnitudes (I used common-magnitude but each px,py,pz could be separate)
AMPLITUDES=[1];

%replicate amplitudes
AMPLITUDES=repmat(AMPLITUDES,1,N);

%% Create DIPOLE ARRAYS with correct intensities & phases corresponding
FF_Approximation = 0;  % NB: visualization surfaces only set to work for NF forms (as I'm setting absolute xyz field positions, and not [theta,phi] angles which would be required for that to work
for i=1:numel(AMPLITUDES)
 	z_dipoles(i)=classMagneticDipole(AMPLITUDES(1),k, 'FF_Approximation', FF_Approximation);
	z_dipoles(i).I_0=1;%AMPLITUDES(1);
	
	x_dipoles(i)=classMagneticDipole(AMPLITUDES(1),k, 'FF_Approximation', FF_Approximation);
	x_dipoles(i).I_0=0;
	
	y_dipoles(i)=classMagneticDipole(AMPLITUDES(1),k, 'FF_Approximation', FF_Approximation);
	y_dipoles(i).I_0=0;
end

%  Collect into one structure
DIPOLES.z_dipoles=z_dipoles;
DIPOLES.x_dipoles=x_dipoles;
DIPOLES.y_dipoles=y_dipoles;



%% DEFINE ARRAYP (Measurement locations, SLICE 1
% ref: sliceView_Demo.m  % This only works with XYZ positions, i.e. NF
% forms of dipoles.

% SLICE 1
% Simulate Varargin; This is an abuse of MATLAB! I know..but it was quick.

Targs={	'ScaleFactor', 3, 'nDim1', nDim1,'nDim2', nDim2, ...
			'Orientation', 'XY', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
			'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
fprintf('\nSlice 1..');	

% Set options
opt.normalize = 1 ;
opt.DoQuiver = 0;

% Compute Observation points (in this plane)
[CLOUD, PLANE]=T.sliceView_getPointCloud(Targs{:});		
ARRAYP=CLOUD.PtCloud; 

% Do calculation
MDSI=EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, DIPOLES); 
field=MDSI.E_c_c;

% make plot
PLANE_VECTOR_PLOT_DEMO(opt, field, Targs, nDim1, nDim2, CLOUD,PLANE)



%% SLICE 2
Targs={	'ScaleFactor', 3, 'nDim1', nDim1,'nDim2', nDim2, ...
			'Orientation', 'XZ', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
			'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
fprintf('\nSlice 2..');	

% Compute Observation points (in this plane)
[CLOUD, PLANE]=T.sliceView_getPointCloud(Targs{:});		
ARRAYP=CLOUD.PtCloud; 

% Do calculation
MDSI=EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, DIPOLES); 
field=MDSI.E_c_c;

% make plot
PLANE_VECTOR_PLOT_DEMO(opt, field, Targs, nDim1, nDim2, CLOUD,PLANE)



%% SLICE 3
Targs={	'ScaleFactor', 3, 'nDim1', nDim1,'nDim2', nDim2, ...
			'Orientation', 'YZ', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
			'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 };
fprintf('\nSlice 3..');		

% Compute Observation points (in this plane)
[CLOUD, PLANE]=T.sliceView_getPointCloud(Targs{:});		
ARRAYP=CLOUD.PtCloud; 

% Do calculation
MDSI=EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, DIPOLES); 
field=MDSI.E_c_c;

% make plot
PLANE_VECTOR_PLOT_DEMO(opt, field, Targs, nDim1, nDim2, CLOUD,PLANE)


%% Finish graph

axis auto
axis vis3d
set(gcf, 'renderer', 'zbuffer')
% set(gcf, 'renderer', 'opengl')
view(124,20)
view(128,14)
view(108,26)

%orbitFlyby('dt', 0.05)
caxis([-20 0])
fprintf('\nDone!\n');
%rgbmap('neon green', 'yellow', 'mustard',200);
%setView('XZ');