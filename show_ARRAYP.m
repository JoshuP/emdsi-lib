function figureHandles=show_ARRAYP(ARRAYP, varargin)
%Given some set of values in ARRAYP, let's see what they actually look
%like.
p=inputParser();
p.addOptional('do3D', 1);
p.addOptional('do2D', 1);
p.parse(varargin{:});
do3D=p.Results.do3D;
do2D=p.Results.do2D;

x=ARRAYP(1,:);
y=ARRAYP(2,:);
z=ARRAYP(3,:);
T=classTransforms;

figureHandles=[];
if do3D
	% plot 3D CUT points
	plotHelper_quickvis 
	% + FIGURE; as above poofs into exisence: FIGURE as figure handle
	FIGURE=gcf;
	plot3(x,y,z,'-bo', 'markersize', 5);
	plot3(x,y,z,'r.', 'markersize', 5);
	title('ARRAYP in 3D', 'fontsize', 20, 'color','black');
	axis auto;
else
	FIGURE=[];
end
figureHandles=[figureHandles FIGURE];

if do2D
	FIGURE=figure;
	grid on; 
	hold on;
	plot(x, 'linestyle', '-', 'marker', '.', 'color', 'r', 'displayname',   'x');
	plot(y, 'linestyle', '-', 'marker', '.', 'color', 'b', 'displayname',   'y');
	plot(z, 'linestyle', '-', 'marker', '.',  'color', 'g', 'displayname', 'z');
	title('ARRAYP (Values) in 2D', 'fontsize', 20);
	xlabel('index');
	ylabel('value');
	h=legend('show','location', 'best');
	axis auto;
else
	FIGURE=[];
end
figureHandles=[figureHandles FIGURE];