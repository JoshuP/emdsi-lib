function dA=computeDifferentialArea(gen_rho, gen_z, varargin)
%function dA=computeDifferentialArea(gen_rho, gen_z, gen_rho_fit, gen_z_fit, varargin)
%7.14.2019 - JP - Remove gen_rho_fit, gen_z_fit - as these are computed internally

% must input the measurement locations gen_rho, gen_z for NF measurement
% along generatrix.

% Visualize 2D Construction plot
%  1/ Show measurement points
%  2/ show bounding thetas for the measurement points that indicate the
%       differential areas
%  3/ further subdivide the differential areas 
%  4/ Do the summation to find the areas for each region using frustum-math
 
% close all;

% INPUTS
%       gen_rho;    1xM rho-coord generatrix for meas. locn, #-of-pts by emp.NP_Smes
%       gen_z;      1xM z-coord generatrix for meas. locn.
%       gen_rho_fit; parametric interpolant for rho
%       gen_z_fit;  parametric interpolant for z
%
% OUTPUTS
%       dA ; scalar vector of floats indicating the surface area of the
%       bounding frustums. M # of points. One area contains each
%       measurement point
%                   NB: VALUE IS NORMALIZED to 2pi.
%                   if we want to correct for dPhi, then mutiply by
%                   (dPhi/(2pi))

global pp

p=inputParser();
p.addOptional('doPlot',0);
p.parse(varargin{:});
doPlot=p.Results.doPlot;

if isempty(gen_rho) & isempty(gen_z) %#ok<AND2>
	method=2; %childBOR
else
	method=1; % Default for Eqab
end

NP=numel(gen_rho);

%Equation assumes that we are on a generatrix in the XZ-plane. Thus X-coordinate is same as rho-value
fn_dA=@(dPhi, x1, x2, z1, z2)(dPhi.*( (x1 + x2)./2) .*  sqrt( (x2-x1).^2+ (z2-z1).^2 )); %Area for frustum bounded by A(x1,z1), B(x2,z2) for a differential azimuth dPhi (radians)


if method==1 % ORIGINAL : BOUNDING FRUSTUMS INDICES
	% original generatrix, M-theta values, NP-phi values [gen_rho;gen_z;]
	% Define interpolant
	% ORIGINAL: JAN 2 2020
	[gen_rho_fit, gen_z_fit, t]=parametericInterpolateGeneratrix(gen_rho, gen_z); %create interpolators based on 'index' position in generator
	si=1;   %index limits of 't' the parameter for the interpolant. Integer values locate measurements.
	ei=NP;
	%bounding theta values for differential Areas
	t_dA=unique([si (si+0.5):1:(ei-0.5) ei]); %'bracketing' theta's for differential-geometries
	gen_rho_dA=gen_rho_fit(t_dA).';
	gen_z_dA=gen_z_fit(t_dA).';
	
	if 0 % Optional plot. Show nodes in black, and boundaries in red
		figure; grid on ;hold on;
		plot(gen_rho, gen_z, '-ko','markersize', 12, 'displayname', 'nodes');
		plot(gen_rho_dA, gen_z_dA, '-rd','markersize', 10, 'displayname', 'bounding vals');
		h=legend('show', 'location', 'best')
		axis equal
	end
	
	%sub-divided Area calculations don't matter on faceted
		N_sublevel=2; %number of 'fine' interpolation between bounding areas
		warning([mfilename ': now asuming FACETED GEOMETRY']);
	
elseif method==2 % Points derive from masterBOR or child-BOR
	 %Array of ti values drawn from map of masterBOR
	ti=pp.BORlib_Smes.childBOR.t; 

	% get indexes of bounding values -> interpolate to get generatrix vals
	t_dA=[];
	for i=1:numel(ti)
		if i==1
			del=ti(i+1)-ti(i);
			t_dA=[t_dA ti(i) ti(i)+del/2]; %first point gets 'lower bound' 
							% & upper_bound to kick things off
		elseif i==numel(ti)
			t_dA=[t_dA ti(end)]; %end point, just gets the last point
		else  %take current point + upper-difference
			del=ti(i+1)-ti(i);
			ti_upper_bound=ti(i)+del/2;
			t_dA=[t_dA ti_upper_bound];
		end
	end
	
	if 1 % interpolate points on (curved)
		masterBOR=pp.BORlib_Smes.masterBOR;
		[gen_rho_dA, gen_z_dA, LCS_dA, GCS_dA]=masterBOR.interp1(t_dA);
		gen_rho_fit=masterBOR.PIM_BOR.generatrix.cfit_rho;
		gen_z_fit=masterBOR.PIM_BOR.generatrix.cfit_z;
		
		% smooth sub-divisions of interpolant on surface: Sublevels
		N_sublevel=100; %number of 'fine' interpolation between bounding areas
		% warning([mfilename ': now asuming CURVED GEOMETRY']); %%----[DEFAULT
		
	else % interpolate points on facet(flat)
		childBOR=pp.BORlib_Smes.childBOR;
		[gen_rho_dA, gen_z_dA, LCS_dA, GCS_dA]=childBOR.interp1(t_dA);
		gen_rho_fit=childBOR.D2.PIM_BOR.generatrix.cfit_rho;
		gen_z_fit=childBOR.D2.PIM_BOR.generatrix.cfit_z;
		
		%sub-divided Area calculations don't matter on faceted
		N_sublevel=2; %number of 'fine' interpolation between bounding areas
		warning([mfilename ': now asuming FACETED GEOMETRY']);
	end
	
	if 0 % Optional plot. Show nodes in black, and boundaries in red
		figure; grid on ;hold on;
		plot(gen_rho, gen_z, '-ko','markersize', 12, 'displayname', 'nodes');
		plot(gen_rho_dA, gen_z_dA, '-rd','markersize', 10, 'displayname', 'bounding vals');
		h=legend('show', 'location', 'best')
		axis equal
	end
	
end

%% Compute N_sublevels for additional accuracy

t_sublevel=[]; %indices for the sublevels.
clear span
for i=1:numel(t_dA)-1
    si_sub=t_dA(i);
    ei_sub=t_dA(i+1);
    idx_interval=linspace(si_sub, ei_sub, N_sublevel); % unique N-sublevels
    t_sublevel=[t_sublevel idx_interval];
    
    % For calculation, note the 
    span(i).t_sublevel	= idx_interval;
    span(i).gen_rho		= gen_rho_fit(idx_interval).'; %interpolators from appropriate curve
    span(i).gen_z		= gen_z_fit(idx_interval).';
	
    for j=1:N_sublevel-1 %find areas of the subdivisions (assume 360degrees)
        x1=span(i).gen_rho(j);
        x2=span(i).gen_rho(j+1);
        z1=span(i).gen_z(j);
        z2=span(i).gen_z(j+1);
        span(i).dAi(j)=fn_dA(2*pi, x1, x2, z1, z2); %Get the frustum area for the entire bounding latitutde
		%span(i).dAi(j)=fn_dA_DEREK(x1, x2, z1, z2); %Get the frustum area for the entire bounding latitutde
    end
    span(i).dA=sum(span(i).dAi);
end

% Calculation is done here for each bounding frustum, summed by subdividing each level.
dA=[span(:).dA];

%% optional - Visualization
if doPlot

    figure('position',[ 691         168         947        1165]); axis equal; xlabel('rho'); ylabel('z-axis'); grid on;
    title('construction plot'); axis([0 0.5 0 1 ]);
    hold on;

    % Measurement points
    if 0
        plot(gen_rho,gen_z, ':ro', 'markersize', 15, 'markerfacecolor','r', 'displayname', 'NSI_{measured}', 'linewidth',2);
    else
        % Shift on-axis points slightly?
        dt=0.25; t_shift=[t(1)+dt t(2:end-1) t(end)-dt];
        gen_rho_shift=gen_rho_fit(t_shift);
        gen_z_zhift=gen_z_fit(t_shift);
        plot(gen_rho_shift,gen_z_zhift, ':ro', 'markersize', 15, 'markerfacecolor','r', 'displayname', 'NSI_{measured}', 'linewidth',2);
    end

    % dA bounding points
    plot(gen_rho_dA, gen_z_dA, 'bo', 'displayname', 'dA (t-direction) boundary demarcations', 'markerfacecolor', 'b', 'markersize',10);

    % sub-levels
    % lines from z-axis to the BOR-surface
    for i=1:numel(t_sublevel)
        x=[0, gen_rho_sublevel(i)];
        z=[gen_z_sublevel(i), gen_z_sublevel(i)];
        plot(x, z, '-k', 'handlevisibility','off');

        x=x(2); %point at BOR marked by a square
        z=z(2);
        plot(x, z, 'g', 'handlevisibility','off','marker', '*', 'markersize', 6,'markerfacecolor','g');

        if i~=numel(t_sublevel); %Skip last one, as there aer no more to draw
            x=[gen_rho_sublevel(i) gen_rho_sublevel(i+1)];
            z=[gen_z_sublevel(i) gen_z_sublevel(i+1)];
            plot(x, z, '-k', 'handlevisibility','off');
        end
    end

    legend show;
	axis auto;
end
    
end