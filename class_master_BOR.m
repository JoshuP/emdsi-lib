classdef class_master_BOR < handle
	%Parent BOR class that one-time computes the generatrix - at some
	%high-precision mesh.
	% sets t_begin, t_end, which can be a large number (ie 1000)
	% all other children BOR specified relative to this
	
	% Will be related to class_BOR_Child
	
	% features
	% t_being=1 up to t_end=NP
	% points are equidistantly spaced along arc (use interparc)
	% trac
	
	
	
	%ref: benchmark_LCS_vs_GCS.m
	%	: define_BOR_Surface.m
	
	properties
		BOR_TYPE
		name
		radius
		
		NP
		
		di=1e-4; %in 'index-units' distance for bracketing points along master generatrix
		di_poles=1e-3; %in 'index-units' distance for
		
		FITTYPE='SPLINE'; %FITTYPE {poly1, poly11 poly2 linearinterp cubic interp smoothingspline lowess}%
		
		% BOR
		PIM_BOR
		
		
		% STRUCTURE---------------------------------------------------
		%   class_BOR_Master with properties:
		%
		%         TYPE: 'SPHERE'
		%       radius: 0.7392
		%           NP: 100
		%           di: 1.0000e-04
		%     di_poles: 1.0000e-03
		%      FITTYPE: 'spline'
		%      PIM_BOR: [1x1 struct]
		%
		%
		%     ....PIM_BOR
		%
		%     generatrix: [1x1 struct]
		%              t: [1x100 double]
		%        gen_rho: [1x100 double]
		%          gen_z: [1x100 double]
		%          sph_r: [1x100 double]
		%      sph_theta: [1x100 double]
		%             dx: [1x100 double]
		%            ddx: [1x100 double]
		%       facet_dx: [1x100 double]
		%      facet_ddx: [1x100 double]
		%            LCS: [1x1 struct]
		%            GCS: [1x1 struct]
		%
		%
		%	....PIM_BOR.generatrix
		%
		%             cfit_t: [1x100 double]
		%           cfit_rho: [1x1 cfit]
		%             cfit_z: [1x1 cfit]
		%         cfit_sph_r: [1x1 cfit]
		%     cfit_sph_theta: [1x1 cfit]
		%            cfit_dx: [1x1 cfit]
	end
	
	methods
		function obj=class_master_BOR(BOR_TYPE, NP, radius, varargin)
			p=inputParser();
			p.addOptional('di', obj.di);
			p.addOptional('di_poles', obj.di_poles);
			p.addOptional('opt_GCS_LCS', 1); %by default do compute the GCS/LCS into PIM_BOR
			p.addOptional('gen_rho_0', []); % for surfNormExpansion, these are the points non-equidistantly spaced on BOR surface
			p.addOptional('gen_z_0', []); 
			p.addOptional('aspectRatio', 2);
			p.parse(varargin{:});
			di=p.Results.di;
			di_poles=p.Results.di_poles;
			opt_GCS_LCS=p.Results.opt_GCS_LCS;
			aspectRatio = p.Results.aspectRatio;
			gen_z_0 = p.Results.gen_z_0;
			gen_rho_0 = p.Results.gen_rho_0; %Eqab
			
			obj.BOR_TYPE=BOR_TYPE;
			obj.NP=NP;
			obj.radius=radius;
			FITTYPE=obj.FITTYPE;
			
            INTERPARC_GENERATE_INTERP_MODE = 'spline'; % good for curves
			% specify raw generatrix points
			switch BOR_TYPE
				case {'Sref', 'surfNormExpansion'}
					% use values that were passed in
					gen_z_0=p.Results.gen_z_0;
					gen_rho_0=p.Results.gen_rho_0;
					
				case {'SPHERE_BOR', 'SPHERE'}
					[gen_z_0, gen_rho_0]=generatrix_circle(radius, NP);  %NB: SPHERE ONLY. (other cases are general!), postscript _0 indicates raw,first pass values from generator function
					%gen_z_0=gen_z_0-min(gen_z_0); 
					%warning([mfilename ': has offset the BOR so that it is entirely above the Z axis - asymmetrically']);
					
				case {'OBLONG_VERTICAL_INTERPARC', 'OVSPHERE'}
					[gen_z_0, gen_rho_0]=generatrix_circle(radius, NP);  %NB: SPHERE ONLY. (other cases are general!), postscript _0 indicates raw,first pass values from generator function
					gen_z_0=gen_z_0*aspectRatio;		%oblong Vertical, 
					
				case {'SQUARE'}
					[gen_z_0, gen_rho_0]=generatrix_square(radius, NP);
					
				case {'OBLONG_HORIZONTAL_INTERPARC', 'OHSPHERE'}
					[gen_z_0, gen_rho_0]=generatrix_circle(radius, NP);  %NB: SPHERE ONLY. (other cases are general!), postscript _0 indicates raw,first pass values from generator function
					gen_rho_0=gen_rho_0*2;		%oblong Vertical
				case 'GENERATRIX_MAUTZBOR1'
					[gen_z_0, gen_rho_0]=generatrix_MautzBOR1(radius,NP);  %NB: SPHERE ONLY. (other cases are general!)
					gen_z_0=(gen_z_0)*radius;
					gen_z_0=gen_z_0-max(gen_z_0)/2; %shift and center June 27,2019
					%gen_rho_0=(gen_rho_0./max(gen_rho_0))*radius; %Set radius to 1 then multiply by desired radius
					gen_rho_0 = gen_rho_0 * radius;
				case 'GENERATRIX_MAUTZBOR2'
					[gen_z_0, gen_rho_0]=generatrix_MautzBOR2(radius,NP);  %NB: SPHERE ONLY. (other cases are general!)
					gen_z_0=(gen_z_0*2)*radius;
					gen_z_0=gen_z_0-radius; %shift and center June 27,2019
					gen_rho_0=(gen_rho_0./max(gen_rho_0))*radius; %Set radius to 1 then multiply by desired radius
				case 'GENERATRIX_MAUTZBOR3'
					[gen_z_0, gen_rho_0]=generatrix_MautzBOR3(radius,NP);  %NB: SPHERE ONLY. (other cases are general!)
					gen_z_0=(gen_z_0*2)*radius;
					gen_z_0=gen_z_0-radius; %shift and center June 27,2019
					gen_rho_0=(gen_rho_0./max(gen_rho_0))*radius; %Set radius to 1 then multiply by desired radius
                case 'CONESPHERE'
                    INTERPARC_GENERATE_INTERP_MODE  = 'none';
                    [gen_z_0, gen_rho_0]=generatrix_Conesphere(radius,NP);
				case 'EQAB'
					gen_z_0=(gen_z_0);
					gen_rho_0 = (gen_rho_0); 
                otherwise
					error([mfilename ': BOR type not yet specified (' BOR_TYPE ')']);
			end
			
			% redistribute points equally along curve
			switch INTERPARC_GENERATE_INTERP_MODE
				case 'none'
					gen_z =  gen_z_0 - mean(gen_z_0); %Center in z-axis;;
					gen_rho = gen_rho_0;
					arclens = sqrt(diff(gen_rho).^2 + diff(gen_z).^2).';
					
				otherwise
					[gen_z, gen_rho, arclens]=INTERPARC_GENERATE(NP,gen_rho_0.',gen_z_0.', [], 'interp_mode', INTERPARC_GENERATE_INTERP_MODE ); %Arclens are the curved segment lengths
			end
			ddx=[0; arclens].'; % deltax relative, between neighbouring points
			dx=[0; cumsum(arclens)].'; %distance from beginning of curve
			
			% MASTER_GENERATRIX
			% EXACT POINTS FROM MASTER_GENERATRIX in
			%		[t, gen_rho, gen_z, sph_r, sph_theta, dx, ddx, facet_dx, facet_ddx]
			% CONTINUOUS-INTERPOLANTS for the following (as a f(t))
			%		[t, rho, z, sph_r, sph_theta,
			
			[~, ~, ~, PIM_BOR]=parametericInterpolateGeneratrix(gen_rho, gen_z,'doDistance', 1 ,'dx', dx,'FITTYPE',FITTYPE); %create interpolators based on 'index' position in generator,
			t=PIM_BOR.generatrix.cfit_t;
			obj.PIM_BOR=PIM_BOR;
			
			% GCS and LCS for all these points
			if opt_GCS_LCS
				obj.get_all_GCS_LCS;
			end
			
		end
		
		function show_generatrix(obj)
		
			ARRAYP=obj.get_master_generatrix();
			figureHandles=show_ARRAYP(ARRAYP);						% Observation points for the CUT
			for i=1:numel(figureHandles);
				figure(figureHandles(i));
				title('Master Generatrix');
			end
		end
		
		
		function ARRAYP=get_master_generatrix(obj)
			%return integer generatrix that specified master bor
			mBOR=obj;
			NP=obj.NP;
			ARRAYP=[mBOR.PIM_BOR.gen_rho; zeros(1,NP); mBOR.PIM_BOR.gen_z];
		end
		
		function [LCS,GCS]=get_all_GCS_LCS(obj)
			index=[1:obj.NP]; %indices to get the GCS & LCS
			[LCS, GCS]=obj.get_GCS_LCS(index);
			obj.PIM_BOR.LCS=LCS;
			obj.PIM_BOR.GCS=GCS;
		end
		
		function [gen_rho_ti, gen_z_ti, LCS_ti, GCS_ti, dx_ti, ddx_ti, sph_r_ti, sph_theta_ti]=interp1(obj, ti)
			% Given vector of (float) indices from the masterBOR, get interps
			% this method for masterBOR, but the child has a similar interface
			% (interpFacet)
			% (class_child_BOR)
			generatrix=obj.PIM_BOR.generatrix;
			gen_rho_ti=generatrix.cfit_rho(ti).';
			gen_z_ti=generatrix.cfit_z(ti).';
			dx_ti=generatrix.cfit_dx(ti).'; 
			ddx_ti=[0 diff(dx_ti)];
			sph_r_ti=generatrix.cfit_sph_r(ti).';
			sph_theta_ti=generatrix.cfit_sph_theta(ti).';
			[LCS_ti, GCS_ti]=obj.get_GCS_LCS(ti);
		end
		
		function [LCS, GCS]=get_GCS_LCS(obj, index)
			% for a specific t=index, return back the LCS & GCS for a SINGLE-POINT on the generatrix.
			% index can be integer or fractional, and scalar or array
			
			
			N=numel(index);
			t_begin=1;
			t_end=obj.NP;
			di=obj.di;
			di_poles=obj.di_poles;
			generatrix=obj.PIM_BOR.generatrix;
			index=roundToPrecision(index, -14); %Numerical noise makes comparing into to float a problem
			
			flag_sanity_check_positive_z_in_last_LCS=0; %on the ti==t_end, (last one) make sure that the z is not inverted.
			
			for i=1:N
				ti=index(i);
				% single point P ;...%ie: P=[gen_rho(i);0; gen_z(i)]; % single [x;y;z] point
				P=[ generatrix.cfit_rho(ti); 0; generatrix.cfit_z(ti)];
				
				if ti==t_begin %First point, at the pole on the bottom
					% bracketing poing juuust above and below
					t_below=ti+di_poles/2;
					t_above=ti+di_poles;
				elseif ti==t_end %Numerical noise issue - matching int to float
					t_below=ti-di_poles;
					t_above=ti-di_poles/2;
					flag_sanity_check=1;
				else
					t_below=ti-di;
					t_above=ti+di;
				end
				P_below=[ generatrix.cfit_rho(t_below); 0; generatrix.cfit_z(t_below)];
				P_above=[ generatrix.cfit_rho(t_above); 0; generatrix.cfit_z(t_above)];
				P_support=[P_below, P_above];
				
				[n_hat_MP, t_hat_MP, phi_hat_MP, ~, ~, GCS_i]=getLCS(P_support, P, 'skipTBarNormalizationCheck', 1);
				if flag_sanity_check_positive_z_in_last_LCS
					if t_hat_MP(3)<0
						error([mfilename ': has caught an inverted LCS on the last point on the generatrix. Numerical matching t_end==float(ti) didnt work']);
					end
				end
				
				% TODO: could put in a check if the last one then there ought to be
				% positive z-component
				
				% In the case any or all of these are not normalized to 1, normalize them.
				% I did this because of small numeric residuals for SPHERE & OVSPHERE
				% but also for non-spherical MAUTZHARRINGTON BOR - showing errors at
				% the poles. This ought to clear it up - but it it not definitive
				TEST=strcmpi(obj.BOR_TYPE, 'SPHERE') || strcmpi(obj.BOR_TYPE, 'OVSPHERE') || strcmpi(obj.BOR_TYPE, 'OHSPHERE') || strcmpi(obj.BOR_TYPE, 'surfNormExpansion'); 
				if ~TEST
					if i==1
						warning([mfilename ': (I haven''t checked yet...may have non-normalized LCS or GCS coordinates. CHECK THIS JOSH']);
					end
				end
				
				n_hat_MP   = n_hat_MP  ./ vecNorm(n_hat_MP);
				t_hat_MP   = t_hat_MP  ./ vecNorm(t_hat_MP);
				phi_hat_MP = phi_hat_MP./ vecNorm(phi_hat_MP);
				
				GCS_i.SPH_R_HAT		= GCS_i.SPH_R_HAT		./ vecNorm(GCS_i.SPH_R_HAT);
				GCS_i.SPH_THETA_HAT	= GCS_i.SPH_THETA_HAT	./ vecNorm(GCS_i.SPH_THETA_HAT);
				GCS_i.SPH_PHI_HAT	= GCS_i.SPH_PHI_HAT		./ vecNorm(GCS_i.SPH_PHI_HAT);
			
					
				
				
				LCS.n_hat_MP(:,i)	=	n_hat_MP;
				LCS.t_hat_MP(:,i)	=	t_hat_MP;
				LCS.phi_hat_MP(:,i) =	phi_hat_MP;
				GCS.SPH_R_HAT(:,i)	=	GCS_i.SPH_R_HAT;
				GCS.SPH_THETA_HAT(:,i)= GCS_i.SPH_THETA_HAT;
				GCS.SPH_PHI_HAT(:,i)=	GCS_i.SPH_PHI_HAT;
			end
		end
		
	end
	
end

