% in this file we compute the magnetic dipoles superposition, and also the
% electric field superposition for a single e-dipole triad and
% single-m-dipole triad.

%% ENVIRONMENT
% close all;
clear all;
T=classTransforms();

%% Options
k0=50; %Wavenumber for magnetic dipole


%% Magnitudes (I used common-magnitude but each px,py,pz could be separate)
electric_dipole_magnitude = 1;
magnetic_dipole_magnitude = 1;


%% orientation of the dipole triplet(s) {c'}, expressed in global-basis
y_hat=[1;0;0]; %Three 3x1 arrays shown for a single dipole. If you had N=100 dipoles, then you need Three 3x100 dipoles for [x_hat, y_hat, z_hat]
x_hat=[0;1;0];
z_hat=[0;0;1];

%% Dipole Triplet location 
ARRAYF = [0;0;0]; % 3x1 array shown.  (N=1, but for N=100 dipoles, then you need 100 separate array entries for each feed location.

%% Electric dipoles
if 0
	I_0=electric_dipole_magnitude;
	PHASE=0; %Can adjust
	FF_Approximation =1; % far field
	E_DIPOLES.x_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
	E_DIPOLES.y_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
	E_DIPOLES.z_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
end

%% Magnetic dipoles
I_0=magnetic_dipole_magnitude;
PHASE=0; %Can adjust phases and magnitudes of each dipole independently.
FF_Approximation =1; % 1=far-field
M_DIPOLES.x_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
M_DIPOLES.y_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
M_DIPOLES.z_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class

%% "ARRAYP" Observation Points (i.e. directions where field is needed)
% you could just have easily created a different array for ARRAYP (3xN for
% "N" observations points. 

% Let's create a circle of points, in the equator (i.e. theta=90deg,
% phi=0:360 deg] of "pointing
DEG2RAD = pi / 180;
N = 100;
thetas = repmat(90.*DEG2RAD, 1, N);
phis = linspace(0.*DEG2RAD, 360.*DEG2RAD, N);
ARRAYP = [thetas;phis];

%% Compute EMDSI superposition at all my observation points.
% EDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, E_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one
MDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, M_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one
MDSI.E_c_s; % Desired output: The far zone fields [E_r; E_theta; E_phi], 
			% where E_r =0 everywhere. 
			% I needed 3xN array for some reason or another, to be
			% compatible with NF, but you can discard first row




 