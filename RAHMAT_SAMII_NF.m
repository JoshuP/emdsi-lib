function [H_c_s, E_c_s, TX_C2S_C, H_c_c, E_c_c]=RAHMAT_SAMII_NF(alpha, beta, gamma, P, f, varargin)
%
%% REVISIONS
% V1.0 | 2021-01-24 | Josh P.      | Creation, derive from: getFieldSubComponent_V2.m
%
%
% ARRAY OF POINTS P, (PAGE) VECTORIZED
%
%
%
%     NEAR FIELD VERSION
%
%
% Given a feed at f, and a point P (where I wish to know the field
% components), and a LCS relationship via [alpha beta gamma], compute
% the E and H in spherical components.
% REF: "UsefulCoordinate Transformations for Antenna Applications
%		Y.RAHMAT-SAMII,
%		IEEE trans. on Ant & Prop, Vol. AP-27, No.4 July 1979
%
% On Notation:
%			 r_c_c		- denotes vector r  in {c}, in cartesian coordinates
%			 rp_c_c		- denotes vector r' in {c}, in cartesian coordinates
%			 rp_cp_c	- denotes vector r' in {c'},in cartesian coordinates
%			 rp_cp_s	- denotes vector r' in {c'},in spherical coordinates (not used in practice for r')

%% Input Parser
p=inputParser();
p.addOptional('TX_C2S_C', []);
p.addOptional('Dipole', 0);
% p.addOptional('EMDSI_SELECTOR', 'MDSI'); %By default compute MDSI
p.parse(varargin{:});
% EMDSI_SELECTOR=p.Results.EMDSI_SELECTOR;
TX_C2S_C=p.Results.TX_C2S_C;
M=p.Results.Dipole; 
T=classTransforms;				% Transform Class

%% Constants
RAD2DEG=180/pi;
DEG2RAD=pi/180;
degC=char(176);
fstr='%.2f';

%% Debug measures
if ~isAValidDipole( M) % Create dummy one if this is not passed in.
	lambda=2e-1; %0.02=2cm
	k_0=2*pi/lambda;
	I_0=1e0;
	M=classMagneticDipole(I_0,k_0); % Magnetic Dipole Class
else
	
end

% Have a Test Rotation, via c_cPrime_Tool
%angle=10; alpha=0*DEG2RAD; beta=0*DEG2RAD; gamma=angle*DEG2RAD;  %ROTATION ABOUT Z

%% STEP 1: Transforms to go back and forth between GCS->LCS (c->c' and
%			vice versa (c'->c)
CP2C=T.TX_CP2C(alpha, beta, gamma); %Update Euler Transform according to the true Euler angles
C2CP=T.TX_C2CP(alpha, beta, gamma);

% OPTIONAL: Show the two systems
	if 0
		[X_c, Y_c, Z_c]=T.getBasisVector();
		fig1=figure('position', [290 540 875 670]);
		T.labelAxes('x', 'y', 'z', 'Rotation of basis-vector points, in \{c\}');
		T.drawABN(     X_c,     Y_c,     Z_c ,'figurehandle', fig1, 'linewidth',3, 'linestyle', '-');
		T.drawABN( CP2C*X_c, CP2C*Y_c, CP2C*Z_c ,'figurehandle', fig1, 'linewidth',3, 'linestyle', ':');
	end

%% STEP 2: Define Point P where I want to compute the field.
	% P is in {c}
	%P=[1,0,0]';		% This r points to a point P, in {c}, and it could become a vectorized array
					% P is a location on the antenna we want to evaluate the field.

%% STEP 3:  Define r_c_c, vector from {c}-origin to pt. P
	r_c_c=P;
	numPts=size(P,2);

%% STEP 4: Define f_c_c, vector from {c}-origin to {c'}-origin
	%f_c_c=[0 0 0]';	% location of the dipole in {c}
	f_c_c=f;			% if f=[0,0,0]', then it is not translated away from origin of {c}

%% STEP 5: calculate r' ; vector from {c'}-origin to pt. P
	rp_c_c=bsxfun(@minus, r_c_c, f_c_c); %rp_c_c=r-f;
	
	if isempty(TX_C2S_C) % means it is the first time this subroutine is called. We need to calculate TX_C2S_C (Once..then it is passed back into this sub, on subsequent calls	
		[~, theta, phi]=T.cart2sph(r_c_c(1,:), r_c_c(2,:), r_c_c(3,:)); % [r, theta,phi]=cart2sph(x,y,z )
	%Transform for CARTESIAN to SPHERICAL in {c} (TAKES LONG: LINE 55)  
		%TX_C2S_C=gpuArray(
		TX_C2S_C=arrayfun(@T.TX_C2S, theta(:), phi(:), 'UniformOutput', 0);%Calculate all 3x3 transformation matrices all at once
		TX_C2S_C=reshape(cell2mat(TX_C2S_C.'),3,3,numPts); % Get each rotation matrix in its own page. 3x3xN
	end
	
%% STEP 6: Calculate rp_cp_c
	rp_cp_c=C2CP*rp_c_c;
	% Spherical Coordinate Components of r' in {c'}
	[rp_cp_sR, rp_cp_sTheta, rp_cp_sPhi]=T.cart2sph(rp_cp_c(1,:), rp_cp_c(2,:), rp_cp_c(3,:));
	rp_cp_s=[rp_cp_sR; rp_cp_sTheta; rp_cp_sPhi]; %Assembled vector
	theta_prime=rp_cp_sTheta;
	phi_prime=rp_cp_sPhi;
%Transform for SPHERICAL to CARTESIAN in {c'} (TAKES LONG: LINE 73)        73:[[TX_S2C_CP=arrayfun(@T.TX_S2C, theta_prime(:), phi_prime(:), %'UniformOutput', 0);%Calculate all 3x3 transformation matrices all at once]]
	TX_S2C_CP=arrayfun(@T.TX_S2C, theta_prime(:), phi_prime(:), 'UniformOutput', 0);%Calculate all 3x3 transformation matrices all at once
	TX_S2C_CP=reshape(cell2mat(TX_S2C_CP.'),3,3,numPts); % Get each rotation matrix in its own page. 3x3xN

%% STEP 7: Calculate Spherical Vector Quantities in {c'};
% rp_cp_s is the spherical coords of P in {c'}..
	[H_r, H_theta, H_phi, E_r, E_theta, E_phi]=M.FieldInSph(rp_cp_s(1,:), rp_cp_s(2,:), rp_cp_s(3,:));
	Hp_cp_s=[H_r; H_theta; H_phi]; %Assemble vectors
	Ep_cp_s=[E_r; E_theta; E_phi];
	
	%Optional plot to show where in the LCS field is being assessed.
	%[x,y,z]=T.sph2cart(rp_cp_s(1,:), rp_cp_s(2,:), rp_cp_s(3,:));
	%figure; plot3(x,y,z,'bo');title('locations where field is made in c'''); axis equal
	
%% STEP 8: All Conversions at once to get Cartesian & Spherical components in {c}
%	H_c_s=TX_C2S_C*CP2C*TX_S2C_CP*Hp_cp_s; % Logic of transforms
%	E_c_s=TX_C2S_C*CP2C*TX_S2C_CP*Ep_cp_s;
% Page Vectorized Calculation
	% ORIGINAL (All at once
	if 0
		TRANSFORM=multiprod(TX_C2S_C, multiprod(CP2C, TX_S2C_CP));
		L=multiprod(TRANSFORM, reshape(Hp_cp_s,3,1,numPts));
		H_c_s=reshape(L, 3,numPts);

		L=multiprod(TRANSFORM, reshape(Ep_cp_s,3,1,numPts));
		E_c_s=reshape(L, 3,numPts);
	end

	% LINEAR METHOD
	TRANSFORM1=multiprod(CP2C, TX_S2C_CP); %Tranform to get back cartesian elements in {c}
	TRANSFORM2=multiprod(TX_C2S_C, TRANSFORM1); %Last additional transform to get back to Spherical in {c}
	L=multiprod(TRANSFORM1, reshape(Hp_cp_s,3,1,numPts)); 
	H_c_c=reshape(L, 3,numPts);	
	L=multiprod(TRANSFORM2, reshape(Hp_cp_s,3,1,numPts));
	H_c_s=reshape(L, 3,numPts);
	
	L=multiprod(TRANSFORM1, reshape(Ep_cp_s,3,1,numPts));
	E_c_c=reshape(L, 3,numPts);	
	L=multiprod(TRANSFORM2, reshape(Ep_cp_s,3,1,numPts));
	E_c_s=reshape(L, 3,numPts);
	

