% NEAR-FIELD CALCULATION SAMPLE
%
% in this file we compute the magnetic dipoles superposition, and also the
% electric field superposition for a single e-dipole triad and
% single-m-dipole triad.

%% ENVIRONMENT
close all;
% clear all;
T=classTransforms();

%% Options
k0=50; %Wavenumber for magnetic dipole
% Sampling Density
nDim1=70;
nDim2=70;

%% Magnitudes
electric_dipole_magnitude = 1;
magnetic_dipole_magnitude = 1;


%% orientation of the diplole triad(s) {c'}, expressed in global-basis
y_hat=[1;0;0]; %Three 3x1 arrays shown for a single dipole. If you had N=100 dipoles, then you need Three 3x100 dipoles for [x_hat, y_hat, z_hat]
x_hat=[0;1;0];
z_hat=[0;0;1];

%% Dipole {c'} orientation 
ARRAYF = [0;0;0]; % 3x1 array shown.  (N=1, but for N=100 dipoles, then you need 100 separate array entries for each feed location.

%% Electric dipoles
I_0=electric_dipole_magnitude; % (I used common-magnitude but each px,py,pz could be separate)
PHASE=0; %Can adjust
FF_Approximation =0; % near field
E_DIPOLES.x_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
E_DIPOLES.y_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
E_DIPOLES.z_dipoles = classElectricDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class


%% Magnetic dipoles
I_0=magnetic_dipole_magnitude; % (I used common-magnitude but each px,py,pz could be separate)
PHASE=0; %Can adjust phases and magnitudes of each dipole independently.
FF_Approximation =0; % 0=near field
M_DIPOLES.x_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
M_DIPOLES.y_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class
M_DIPOLES.z_dipoles = classMagneticDipole(I_0, k0, 'phase', PHASE, 'FF_Approximation', FF_Approximation);% Electric Dipole Class

%% "ARRAYP" Observation Points (i.e. location where field is needed)

% Here I'm using a tool I made to give an array of observatoin points in a
% plane. See if you can figure it out. I put in tools to control scale,
% number of points, in which orientation I wanted it, elemental shifts dAX,
% etc, as well as option to rotate the plane in various ways. (sequentially
% applied in the order one might hope ;) ).
[CLOUD, PLANE]=T.sliceView_getPointCloud( 'ScaleFactor', 3, 'nDim1', nDim1,'nDim2', nDim2, ...
			'Orientation', 'XY', 'dAX', 0, 'dAY', 0,'dAZ', 0, ...
			'dPhi', 00,'dTheta', 0,'dX', 0,'dY', 0,'dZ', 0 );		
ARRAYP=CLOUD.PtCloud;

% you could just have easily created a different array for ARRAYP (3xN for
% "N" observations points. 

%% Compute EMDSI superposition at all my observation points.
EDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, E_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one
MDSI = EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, M_DIPOLES); % ARRAYP output follows the 4900 points order of ARRAYP, one for one
EMDSI_FIELD = EDSI.E_c_c + MDSI.E_c_c;
% Field is represented in global basis [Ex; Ey; Ez] on a cartesian basis

% Spherical basis would be 
% EDSI.E_c_s % Spherical basis [E_r; E_theta; E_phi].'
% MDSI.E_c_s % Spherical basis [E_r; E_theta; E_phi].'



 