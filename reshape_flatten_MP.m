function out=reshape_flatten_MP(ARRAY, NumRows, NP, M)
%function out=reshape_flatten_MP(ARRAY, NumRows, NP, M)
out=reshape(ARRAY, NumRows, M*NP); % Flatten an NP array
