function [result]=setVectorOrientation(vec, varargin)
%function [result]=setVectorOrientation(vec, varargin)
%GOAL: Check if is the required orientation; and set it  that way if it
%wasn't.
%Default: 'orientation'={'horizontal', 'vertical'}

p=inputParser;
defaultOrientation='vertical';
p.addOptional('orientation',defaultOrientation); 
parse(p,varargin{:});
orientation=p.Results.orientation;

%Check if we are allowed to resize this vector at all. One of the
%dimensiosn has to be 1
numRows=size(vec,1);
numCols=size(vec,2);

%if vector was empty; this is a meaningless operation. Do it anyway;
if (size(vec,1)==0) && (size(vec,2)==0);
    result=vec;
    return;
end

if length(size(vec))~=2
    error([mfilename ' can only be called with a 2 dimensional array']);
end

if (numRows~=1) && (numCols~=1) %if both are not dimension 1
    error([mfilename ' can only be called with an array of at least 1 singleton dimension']);
end

%choose the orientation we want;
switch lower(orientation)
    case {'row', 'horizontal'};
        if numCols>numRows; %then it's already in the right way
           result=vec;
        else
            result=vec.';
        end
case {'column', 'vertical'}
        if numCols<numRows; %then it's already in the right way
           result=vec;
        else
            result=vec.';
        end
end
%Input