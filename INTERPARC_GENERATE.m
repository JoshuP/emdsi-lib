function [gen_z,gen_rho, chordlen_actual]=INTERPARC_GENERATE(Npts,x_s,z_s, INDICES, varargin)
% see; class_NSI_Data.m, method getGeneratrix, July 3rd, 2019
%Given points along the generatrix, evaluate gen_rho and gen_z
%uniformly spaced along the generatrix using interparc (decimates a
%curve into N equal points)
% cumarc = cumulative arc length

p=inputParser();
p.addOptional('interp_mode', 'spline');
p.parse(varargin{:});
interp_mode = p.Results.interp_mode  ;


N=Npts;

if isempty(INDICES) %Then take N  points equally spaced along SPLINE (PIM-BOR)
	%N=linspace(0,1,N);
	
	% Jan 1, adding chordlen_actual
	[pt, ~,~,chordlen_actual] = interparc(N,x_s,z_s,'spline'); %Spline interpolate equidistant points
	
	%ORIGINAL;
	%pt = interparc(N,x_s,z_s,'spline')'; %Spline interpolate equidistant points
	pt=pt.';
	gen_rho=pt(1,:);
	gen_z=pt(2,:);
else
	% INterpret the array to indicate relative spacing between elements
	% IF there are N
	%Reference: https://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
	
	% given [my_A, my_B], scale those to the range that the PIM-bor is
	% defined on. (remember; integer values)
	
	% To make this AGNOSTIC to the sampling grid specified at measurement
	% time from NSI, we need to take special measures
	
	% Remember; they sample more near the north-pole, so there might be 20
	% integer indexxes right near the top, but this is not spaced
	% uniformly along generatrix.
	% Let's make a new interpolant with uniform spacing integer values,
	% then draw our 'chirped' sample-set from that new
	% uniform_indexed_interpolant (UII-BOR)
	
	%NFM
	gen_rho_nfm=x_s;
	gen_z_nfm=z_s;
	
	% DEFINE UII-BOR interpolant (100 uniformly spaced points
	%N=max([numel(t), 1000]); %
	N=1000;
	
	%ORIGINAL
	%pt = interparc(N,x_s,z_s,'spline')'; %Spline interpolate equidistant points
	
	
	% Jan 1, adding chordlen_actual
	[pt, ~,~,chordlen_actual] = interparc(N,x_s,z_s,interp_mode); %Spline interpolate equidistant points
	pt=pt.';
	
	gen_rho_uii=pt(1,:);
	gen_z_uii=pt(2,:);
	
	% UII-MAKE A GENERATRIX FOR THESE POINTS
	[cfit_rho_uii, cfit_z_uii, t_uii]=parametericInterpolateGeneratrix(gen_rho_uii, gen_z_uii); %create interpolators based on 'index' position in generator
	
	% Rescale our INDICES to fit inside these POINTS
	a=min(t_uii);
	b=max(t_uii);
	myA=min(INDICES);
	myB=max(INDICES);
	rescaler=@(x,a,b, myA, myB)( ( (b-a)*(x-myA) )./(myB-myA) + a  ); %See reference to rescale my array to the index-range
	t_chirp=rescaler(INDICES, a, b, myA, myB);% GENERALLY Non-uniformly spaced Index points along the curve 1:t(end)
	% Array of t-indexxed positions into UII where the spacing in INDICES
	% is reflected in our t_final. I can imprint a 'chirp' now on the
	% measured locations
	
	% Sample our points now on the generatrix, on the
	x_s=cfit_rho_uii(t_chirp);
	z_s=cfit_z_uii(t_chirp);
	
	gen_rho=x_s.';
	gen_z=z_s.';
	if 00; % Constructin plot showing the ponits
		figure; grid on; hold on;
		plot(gen_rho_nfm, gen_z_nfm, '-ro', 'displayname', 'nfm-bor', 'markerfacecolor', 'r', 'markersize', 5);
		plot(gen_rho_uii, gen_z_uii, 'kd', 'displayname', 'uii-bor', 'markersize', 2, 'markerfacecolor','k');
		plot(gen_rho, gen_z, '-bo', 'displayname', 'pec-bor')
		legend show
		axis equal
	end
end