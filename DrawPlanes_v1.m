function [ out ] = DrawPlanes_v1( varargin )
%add option to BlankFaces, if plotted
p=inputParser;
p.addOptional('plotXyzDots',0);
p.addOptional('plotPlanes',1);
p.addOptional('ScaleFactor', 1);
p.addOptional('BlankFaces', 0);
p.addOptional('plane1Label', 'XZ-Plane');
p.addOptional('plane2Label', 'YZ-Plane');
p.addOptional('plane3Label', 'XY-Plane');
p.addOptional('cutOverRide', 0); % if this is 1; label for principle cuts


p.parse(varargin{:});
plotXyzDots=p.Results.plotXyzDots;
plotPlanes=p.Results.plotPlanes;
ScaleFactor=p.Results.ScaleFactor;
BlankFaces=p.Results.BlankFaces;
plane1Label=p.Results.plane1Label;
plane2Label=p.Results.plane2Label;
plane3Label=p.Results.plane3Label;

cutOverRide=p.Results.cutOverRide;
if cutOverRide;
	plane1Label='\phi=0\circ cut';
	plane2Label='\phi=90\circ cut';
	plane3Label='\theta=90\circ cut';
end

hold on;
if plotPlanes

	axis([-1 1 -1 1 -1 1]*.6);
	func = @(x) colorspace('RGB->Lab',x);
	c = distinguishable_colors(4,'w',func);

	%PLANE 1 (XZ)
	A=[-1 0 -1].*ScaleFactor;
	B=[-1 0 1].*ScaleFactor;
	C=[1 0 1].*ScaleFactor;
	D=[1 0 -1].*ScaleFactor;

	x=[A(1); B(1); C(1); D(1)];
	y=[A(2); B(2); C(2); D(2)];
	z=[A(3); B(3); C(3); D(3)];
	p=patch(x,y,z,'r'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
	set(p, 'facelighting', 'none')
	set(p,'facecolor','r')
	set(p,'facealpha',0.1)
	%set(p,'facealpha', 0.5)
	axis auto;
	set(p,'specularColorReflectance',0.0)
	out.textlabel(1)=text(D(1), D(2), D(3), plane1Label, 'fontsize', 20);%usually XZ
	out.p(1)=p;
	
	%PLANE 2 (YZ)
	A=[0 -1 -1].*ScaleFactor;
	B=[0 -1 1].*ScaleFactor;
	C=[0 1 1].*ScaleFactor;
	D=[0 1 -1].*ScaleFactor;
	
	
	x=[A(1); B(1); C(1); D(1)];
	y=[A(2); B(2); C(2); D(2)];
	z=[A(3); B(3); C(3); D(3)];
	p=patch(x,y,z,'w'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
	set(p, 'facelighting', 'none')
	set(p,'facecolor','b')
	set(p,'facealpha',0.1)
	out.p(2)=p;
	%set(p,'facealpha', 0.5)
	axis auto;
	set(p,'specularColorReflectance',0.0)
	out.textlabel(2)=text(D(1), D(2), D(3), plane2Label, 'fontsize', 20);%usually YZ


	%PLANE 3 (XY)
	A=[-1 -1 0].*ScaleFactor;
	B=[-1 1 0].*ScaleFactor;
	C=[1 1 0].*ScaleFactor;
	D=[1 -1 0].*ScaleFactor;

	x=[A(1); B(1); C(1); D(1)];
	y=[A(2); B(2); C(2); D(2)];
	z=[A(3); B(3); C(3); D(3)];
	p=patch(x,y,z,'w'); %, 'markerfacecolor', 'flat', 'facecolor', 'red');
	set(p, 'facelighting', 'none')
	set(p,'facecolor','g')
	set(p,'facealpha',0.1)
	out.p(3)=p;
	%set(p,'facealpha', 0.5)
	axis auto;
	set(p,'specularColorReflectance',0.0)
	out.textlabel(3)=text(C(1), C(2),C(3), plane3Label, 'fontsize', 20); %usually XY

	%axis equal
	%box off
	
	if BlankFaces
		for i=1:3; 
			set(out.p(i),'facealpha',0);
			set(out.p(i),'facecolor','none');
		end
	end
end


if plotXyzDots
	% Additional construction plot mark-up
	T=classTransforms;  [xH,yH,zH]=T.getBasisVector;
	sf=1.2.*ScaleFactor; 
	xH=xH*sf; yH=yH*sf; zH=zH*sf;
	hold on;
	T.plotPoint(xH, 'plotDetails', {'-go', 'linewidth', 10}, 'Constructionlines', 1);
	T.plotPoint(yH, 'plotDetails', {'-go', 'linewidth', 10}, 'Constructionlines', 1);
	T.plotPoint(zH, 'plotDetails', {'-go', 'linewidth', 10}, 'Constructionlines', 1);
	set(gcf,'color', 'w')
end
