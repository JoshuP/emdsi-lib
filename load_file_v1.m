function [out]=load_file_v1(filename)
% Function to load one of two datasets, as in email on Wed., Mar. 31, 2021
%filename = '2021-03-31-Total-E-Field.txt';
%filename = '2021-03-31-Total-H-Field.txt';


% CONSTANTS
RAD2DEG = 180/pi;
DEG2RAD = pi / 180;
T=classTransforms;

% HEADER INFORMATION & LAYOUT
%                    LOCATION                               EH_R                 EH_THETA                EH_PHI 
% medium      R/m         THETA         PHI          magn.     phase      magn.     phase      magn.     phase

% Load datafile
t=readtable(filename);
varNames = {'medium', 'R_m', 'THETA', 'PHI', 'EH_r_mag', 'EH_r_phase', 'EH_theta_mag', 'EH_theta_phase', 'EH_phi_mag', 'EH_phi_phase'};
t.Properties.VariableNames = varNames; % names of columns

% Delete duplicate data at phi=360
warning on
warning([filename ' deleting duplicate data at PHI=360']);
warning off
toDelete = t.PHI==360;
t(toDelete,:) = [];


% Convert to radians and make into row-vectors
numPts = size(t,1);
THETA= t.THETA.'.*DEG2RAD; % convert to 1xn_rows, in radians
PHI = t.PHI.'.*DEG2RAD;
R = t.R_m.';
ARRAYF_2D_TNP=[R; THETA; PHI];

% Convert the measurement locations into cartesian locations
[x, y, z]=T.sph2cart(R, THETA, PHI);
ARRAYF_2D_XYZ= [x;y;z];

if 0 % visualize the measurements?
	figure; plot3(x,y,z,'r.', 'markersize', 2);
	axis vis3d
	grid on;
	title(['Measurment locations in: ' filename]);
	xlabel('X'); ylabel('Y'); zlabel('Z');
end

% Read and convert the field values to SPHERICAL COMPLEX FORM, in the {c} basis
% Convert Magnitude and Angle to Complex (Re+Imag), for E_s= [E_r;E_theta;E_phi]
EH_c_s = [ t.EH_r_mag.'		.* exp(1i.*t.EH_r_phase).'; ...
		t.EH_theta_mag.' .* exp(1i.*t.EH_theta_phase).'; ...
		t.EH_phi_mag.'	.* exp(1i.*t.EH_phi_phase).'; ...
		];

% Convert Field quantities in SPHERICAL basis to Cartesian
theta=THETA;
phi=PHI;
TX_S2C_C=arrayfun(@T.TX_S2C, theta(:), phi(:), 'UniformOutput', 0);%Calculate all 3x3 transformation matrices all at once, theta & phi are the
TX_S2C_C=reshape(cell2mat(TX_S2C_C.'),3,3,numPts); % Get each rotation matrix in its own page. 3x3xN
EH_c_c = multiprod(TX_S2C_C, reshape(EH_c_s,3,1,numPts));  % Then Fields will be obtained on cartesian basis
EH_c_c = reshape(EH_c_c, 3, numPts,1); % Now arranged in same geometry as ARRAYF_2D_XYZ.  COMPLEX CARTESIAN VALUE


% Outputs
out.ARRAYF_2D_TNP = ARRAYF_2D_TNP; % the spherical coordinates of the measurements
out.ARRAYF_2D_XYZ = ARRAYF_2D_XYZ; % the cartesian coordinates of the measurements
out.FIELD_c_s = EH_c_s; % The sampled field, in {c}-basis, spherical
out.FIELD_c_c = EH_c_c; %Sampled field in{c}-basis, cartesian
out.TABLE = t; 
