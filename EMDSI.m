function OUT=EMDSI(ARRAYF, ARRAYP, x_hat, y_hat, z_hat, DIPOLES, varargin)
% FNNAME   EMDSI
%
% INPUT: 
%		ARRAYF: 3xN array of floats of the cartesian position of the source dipoles [x;y;z].'
%		ARRAYP: 3xN array of floats indicating cartesian positions to compute the field,  [x;y;z].'
%				OR
%				2xN array of [theta,phi].' floats (in radians) indicating Far-zone observation directions
%		x_hat: 3xN orthonormal basis vector [x;y;z].' indicating orientation of px dipole component, if, for example aligned to global-system then [1;0;0].'
%		y_hat: 3xN orthonormal basis vector [x;y;z].' indicating orientation of py dipole component, if, for example aligned to global-system then [0;1;0].'
%		z_hat: 3xN orthonormal basis vector [x;y;z].' indicating orientation of pz dipole component, if, for example aligned to global-system then [0;0;1].'
%		DIPOLES is a 1xN array of classElectricDipoles or classMagneticDipoles, and must have 
%				DIPOLES.x_dipoles &
%				DIPOLES.y_dipoles & 
%				DIPOLES.z_dipoles
%
% OUTPUT:
%          
% Modified: 24-Jan-2021 19:19:08
% Author: Josh Philipson
% ENVIRONMENT: % ENVIRONMENT: Curve Fitting ToolboxData Acquisition ToolboxInstrument Control ToolboxMATLABMATLAB CompilerSignal Processing Toolbox v3.5.114.14.29.88.08.4 rel:(R2020a)(R2020a)(R2020a)(R2020a)(R2020a)(R2020a), 18-Nov-201918-Nov-201918-Nov-201918-Nov-201918-Nov-201918-Nov-2019

%% VERSIONING
%             Author: Josh P.
%      Creation date: 24-Jan-2021 19:18:58
%             Matlab: 9.8(R2020a)
%  Required Products: 
% 
%% REVISIONS
% V1.0 | 2021-01-24 | Josh P.      | Creation, derive from: getElementContributions_V2.m
%


% GOAL: Create a single Magnetic dipole at the origin and create a plot
% this dipole will have only a pz component

% given: p= px (x_hat) + py (y_hat) + pz (z_hat), only pz ~= 0
% x_hat

% Josh: 
% n_hat_BOR -->		x_hat, by default aligned to global-system as [1;0;0].'
% phi_hat_BOR -->	y_hat, by default aligned to global-system as [0;1;0].'
% t_hat_BOR -->		z_hat, by default aligned to global-system as [0;0;1].'
%
% DEFINE: x_hat, y_hat_, z_hat as the basis vectors setting the oritnation
% of the total dipole p, p=px (x_hat) + py (y_hat) + pz (z_hat)
%
% Previous; had DIPOLES.t_dipoles & DIPOLES.phi_dipoles, 
% now have: % DIPOLES.x_dipoles, .y_dipoles, .z_dipoles
%
%
% Function used to compute each feed element (F) contributions to each
% observation point, or angle, P
%
% ARRAYF - Feed positions (xyz)
% ARRAYP - Observation positions (xyz), or angles(theta,phi)
% LCS is next 3 args
% Dioples
%
%
% OUT values returned are in SPHERICAL and CARTESIAN BASIS in {c}
%
%   Jan 24 2021, Created variant for Derek McNamara - will assume we need
%   to sum 3 dipoles p=px (x-hat) + py (y-hat) + pz (z-hat)

%
%	May 8 2019.  
%	 NOTE: 
%		-for Near-field form (i.e. M.FF_Approximation=0) ARRAYP is a
%		 3xn series of [x,y,z]' coordinates
%		-For Far-field form (i.e. M.FF_Approximation=1) ARRAYP is 
% 

% getFieldSubComponent can get vectorized results for an array of
% locations at P

% NOTE SUBSCRIPTS
%  [E|H]_[c|cp]_[c|s]_[z|x|y] = [E or H field]
%								[global = c, or lcs=cprime]
%								[c=cartesian, or spherical
%								[z=z-direction or x-direction or y-direction]

p=inputParser();
p.addOptional('verbose', 1);
p.parse(varargin{:});
verbose=p.Results.verbose;

%% Preallocate arrays
[~,m]=size(ARRAYP); % Number of elements
blank_size=zeros([3,m]);

% Arrays to track the z-hat dipoles
E_c_s_z_SUM=blank_size; % Spherical Components 
H_c_s_z_SUM=blank_size;

E_c_c_z_SUM=(blank_size); % CARTESIAN Components
H_c_c_z_SUM=(blank_size);

% Arrays to track the x-hat dipoles
E_c_s_x_SUM=(blank_size); % SPHERICAL
H_c_s_x_SUM=(blank_size);

E_c_c_x_SUM=(blank_size); % CARTESIAN
H_c_c_x_SUM=(blank_size);

% Arrays to track the y-hat dipoles
E_c_s_y_SUM=(blank_size); % SPHERICAL
H_c_s_y_SUM=(blank_size);

E_c_c_y_SUM=(blank_size); % CARTESIAN
H_c_c_y_SUM=(blank_size);


% Initialize the resultant arrays
E_c_s=(blank_size); % SPHERICAL
H_c_s=(blank_size);

E_c_c=blank_size; % CARTESIAN
H_c_c=blank_size;

T=classTransforms;
numF=size(ARRAYF, 2);

TX_C2S_C=[];% Array that allows transform from Cartesian To Spherical in {c}.  
			% First time we don't know it, but we have this global transform every other time.


% SELECT Nearfield or Farfield ?
type=class(DIPOLES.x_dipoles(1));
if DIPOLES.x_dipoles(1).FF_Approximation==0 %Then NEAR FIELD
	type='Near Field Mode';
else %use far field
	type='Far Field Mode';
end
if verbose
	if isa(DIPOLES.x_dipoles(1),'classElectricDipole')
		fprintf('    EDSI now running  (%s)\n', type);
		
	elseif isa(DIPOLES.x_dipoles(1),'classMagneticDipole')
		fprintf('    MDSI now running (%s)\n', type);
	end
end

%% Loop
%print out at desired percentages;
% desPct=10:100;
startic=tic;

for iF=1:numF %Each F (feed) contributes to the field everywhere (all P).
	
	Percentage=iF/numF*100;
	if ~mod(iF,1000) ||iF==10
		soFar=toc(startic); %time in seconds to do iF counts
		TimePer_iF=soFar./iF; %Each one took this long, on average.
		projectedTotalTime=TimePer_iF*numF;  %at that rate, all of 'em take..
		projectedTimeRemaining=(numF-iF).*TimePer_iF; %In seconds
		hrs_left=floor(projectedTimeRemaining/3600);
		min_left=floor((projectedTimeRemaining-hrs_left*3600)/60);
		sec_left=projectedTimeRemaining-hrs_left*3600-min_left*60;
		
		if verbose
			disp(['          ..processing feed: ' num2str(iF, '%05.0f') '/' num2str(numF, '%05.0f') '      (' num2str(Percentage, '%02.0f') '% complete)' ...
				'....(' num2str(hrs_left, '%02.0f') 'h:' num2str(min_left, '%02.0f') 'm:' num2str(sec_left, '%02.0f')  's remaining)']);
		end
	end
	
	
	Fi=ARRAYF(:,iF);  %The exact feed coordinates we are considering right now
		% Analyze the x-component, 
		% In this case the LCS of the iF-th Feed component.

		%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
		% x-dipole component [xp,yp,zp]<==[-z_hat, y_hat, x_hat]=[r,b,g]
		if 1
			xp=-z_hat(:,iF);  %x'=xp=xprime ORIGINAL
			yp= y_hat(:,iF);
			zp= x_hat(:,iF);
			
			[alpha, beta, gamma]=T.getEulerAngles(xp, yp, zp, 'invert', 1);
			%  Invert like this: yielding the EulerAngles to take {c}-->{c'} other
			%  wise we have the alpha,beta,gamma to take primed coords back to GCS
					
			M=DIPOLES.x_dipoles(iF);
			%TX_C2S_C=[];
			% Near-field or Far-field calculation?
			if ~M.FF_Approximation  % Near
				[H_c_s_x, E_c_s_x, TX_C2S_C, H_c_c_x, E_c_c_x]=RAHMAT_SAMII_NF(alpha, beta, gamma, ARRAYP, Fi,  'TX_C2S_C', TX_C2S_C, 'Dipole',M);%contributions from this element to all P locations.
			else 					% Far
				%TX_C2S_C=[]; warning([mfilename ': setting TX_C2S_C=[]']);
				[H_c_s_x, E_c_s_x, TX_C2S_C, H_c_c_x, E_c_c_x]=RAHMAT_SAMII_FF(alpha, beta, gamma, ARRAYP, Fi, 'TX_C2S_C', TX_C2S_C, 'Dipole',M); %contributions from this element to all P locations
			end
			
		else
			H_c_s_x=0; % Spherical components
			E_c_s_x=0;
			
			H_c_c_z=0;% Cartesian components
			E_c_c_z=0;
		end
		
		
		%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
		%y-dipole component [xp,yp,zp]<==[x_hat, -z_hat, y_hat]=[g,r,b]
		if 1
			xp= x_hat(:,iF);  %x'=xp=xprime ORIGINAL
			yp=-z_hat(:,iF);
			zp= y_hat(:,iF);
			
			[alpha, beta, gamma]=T.getEulerAngles(xp, yp, zp, 'invert', 1);
			%  Invert like this: yielding the EulerAngles to take {c}-->{c'} other
			%  wise we have the alpha,beta,gamma to take primed coords back to GCS
					
			M=DIPOLES.y_dipoles(iF);
			%TX_C2S_C=[];
			% Near-field or Far-field calculation?
			if ~M.FF_Approximation  % Near
				[H_c_s_y, E_c_s_y, TX_C2S_C, H_c_c_y, E_c_c_y]=RAHMAT_SAMII_NF(alpha, beta, gamma, ARRAYP, Fi,  'TX_C2S_C', TX_C2S_C, 'Dipole',M);%contributions from this element to all P locations.
			else 					% Far
				%TX_C2S_C=[]; warning([mfilename ': setting TX_C2S_C=[]']);
				[H_c_s_y, E_c_s_y, TX_C2S_C, H_c_c_y, E_c_c_y]=RAHMAT_SAMII_FF(alpha, beta, gamma, ARRAYP, Fi, 'TX_C2S_C', TX_C2S_C, 'Dipole',M); %contributions from this element to all P locations
			end
			
		else
			H_c_s_y=0; % Spherical components
			E_c_s_y=0;
			
			H_c_c_y=0;% Cartesian components
			E_c_c_y=0;
		end
		
		
		%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
		% z-dipole component [xp,yp,zp]]==[x_hat_prime,y_hat_prime,z_hat_prime]<==[x_hat, y_hat, z_hat]=[r,b,g] oriented along LCS Z
		if 1
			xp=x_hat(:,iF);  %x'=xp=xprime = x_hat_prime, alignment of the z-component of our dipole for the pz-component, aligns the dipoles LCS [x',y',z']=[x_hat,y_hat,z_hat]
			yp=y_hat(:,iF);
			zp=z_hat(:,iF);
			
			[alpha, beta, gamma]=T.getEulerAngles(xp, yp, zp, 'invert', 1);
			%  Invert like this: yielding the EulerAngles to take {c}-->{c'} other
			%  wise we have the alpha,beta,gamma to take primed coords back to GCS
			
			M=DIPOLES.z_dipoles(iF);
			
			% Near-field or Far-field calculation?
			if ~M.FF_Approximation  % Near
				[H_c_s_z, E_c_s_z, TX_C2S_C, H_c_c_z, E_c_c_z]=RAHMAT_SAMII_NF(alpha, beta, gamma, ARRAYP, Fi, 'TX_C2S_C', TX_C2S_C, 'Dipole',M); %contributions from this element to all P locations
			else %					% Far
				%TX_C2S_C=[]; warning([mfilename ': setting TX_C2S_C=[]']);
				[H_c_s_z, E_c_s_z, TX_C2S_C, H_c_c_z, E_c_c_z]=RAHMAT_SAMII_FF(alpha, beta, gamma, ARRAYP, Fi, 'TX_C2S_C', TX_C2S_C, 'Dipole',M); %contributions from this element to all P locations
			end
			
		else
			H_c_s_z=0; % Spherical components
			E_c_s_z=0;
			
			H_c_c_z=0; % Cartesian components
			E_c_c_z=0;
		end
		
		
		
		%Collect the z-hat contributions from all elements computed so far, to the z-contribution of E&H
		E_c_s_z_SUM =  E_c_s_z_SUM  +  E_c_s_z; %SPHERICAL
		H_c_s_z_SUM =  H_c_s_z_SUM  +  H_c_s_z;
		
		E_c_c_z_SUM =  E_c_c_z_SUM  +  E_c_c_z; %CART
		H_c_c_z_SUM =  H_c_c_z_SUM  +  H_c_c_z;
		
		
		%Collect the x-hat contributions from all elements computed so far, to the x-contribution of E&H so far
		E_c_s_x_SUM = E_c_s_x_SUM  +  E_c_s_x; %SPH
		H_c_s_x_SUM = H_c_s_x_SUM  +  H_c_s_x;
		
		E_c_c_x_SUM = E_c_c_x_SUM  +  E_c_c_x;% CART
		H_c_c_x_SUM = H_c_c_x_SUM  +  H_c_c_x;
		
		%Collect the y-hat contributions from all elements computed so far, to the y-contribution of E&H so far
		E_c_s_y_SUM = E_c_s_y_SUM  +  E_c_s_y; %SPH
		H_c_s_y_SUM = H_c_s_y_SUM  +  H_c_s_y;
		
		E_c_c_y_SUM = E_c_c_y_SUM  +  E_c_c_y;% CART
		H_c_c_y_SUM = H_c_c_y_SUM  +  H_c_c_y;
end



% Accumulate the total E&H  field
% NO ILLEGAL OPERATIONS
E_c_s = (E_c_s_z_SUM) + (E_c_s_x_SUM) + (E_c_s_y_SUM);
H_c_s = (H_c_s_z_SUM) + (H_c_s_x_SUM) + (H_c_s_y_SUM);

E_c_c = (E_c_c_z_SUM) + (E_c_c_x_SUM) + (E_c_c_y_SUM);
H_c_c = (H_c_c_z_SUM) + (H_c_c_x_SUM) + (H_c_c_y_SUM);


OUT.E_c_s=E_c_s; % SPH
OUT.H_c_s=H_c_s;

OUT.E_c_c=E_c_c; % CART
OUT.H_c_c=H_c_c;

if verbose
		if isa(DIPOLES.z_dipoles(1),'classElectricDipole')
		disp(['    EDSI Finished!']);
	elseif isa(DIPOLES.z_dipoles(1),'classMagneticDipole')
		disp(['    MDSI Finished!']);
	end
end