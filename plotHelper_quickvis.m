figure('Position',[707.00, 548.00, 1140.00, 950.00]);
grid on ;
hold on; 

DrawPlanes_v1('ScaleFactor', 1, 'BlankFaces', 0, 'plane1Label', '\phi=0\circ cut', 'plane2Label', '\phi=90\circ cut', 'plane3Label', '\theta=90\circ cut' );
T.drawBasisVectors
axis off;
axis vis3d;
