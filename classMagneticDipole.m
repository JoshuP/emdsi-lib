classdef classMagneticDipole < handle
	% classMagneticDipole handles computation of the dipole 
	
	properties
		I_0; % Peak Current, I(t)=I_0 * cos(w*t)
		k_0
		FF_Approximation=0;
		%COMPUTED
		epsilon_0 %=8.854187817e-12; % F/m
		
		% TRUE
		mu_0= 4*pi*10^-7; %Permeability of Free space [ N/ A^2]
		
		eta_0; 
		c=2.99792458e8
		phase=0;
		
	end
	
	methods
		function obj=classMagneticDipole(I_0, k_0, varargin)
			p=inputParser();
			p.addOptional('phase',0);
			p.addOptional('FF_Approximation',0);
			
			p.parse(varargin{:});
			obj.phase=p.Results.phase;
			obj.FF_Approximation=p.Results.FF_Approximation;
			
			obj.I_0=I_0;
			obj.k_0=k_0;

			obj.epsilon_0= 1/ (obj.mu_0*obj.c^2);% epsilon_0~8.854187817620... � 10-12 
			obj.eta_0=sqrt(obj.mu_0/obj.epsilon_0);
		end
		
		function [H_r, H_theta, H_phi, E_r, E_theta, E_phi]=FieldInSph(obj, r, theta, phi)
			% Computes the field coordinates in cartesian coordinates
			% theta is declination angle
			% output field component is always in the phi-hat direction in 
			%        coordinate system of the dipole
			% Theta in radians
			
			eta0=obj.eta_0;
			
			k0=obj.k_0;
			I0=obj.I_0;
			%disp(['I0=' num2str(I0)]);
			
			if ~obj.FF_Approximation  %Use Near-Field form

				E=exp(-1j.*k0.*r - (+1j*obj.phase) );						%warning([mfilename ': testing out negative phase in SMD (April 10)']);
				
				H_r		=(I0/(2*pi*eta0)) .*   cos(theta).*( (1./r.^2) + 1./(1j.*k0.*r.^3) ) .* E ;
				H_theta =((1j.*k0.*I0)/(4*pi*eta0)) .* sin(theta) .* ( (1./r) + 1./(1j.*k0.*r.^2) - 1./(k0.^2.*r.^3) ) .* E; 
				H_phi		= zeros(size(H_r));

				E_r			=zeros(1,numel(r));
				E_theta		=zeros(1,numel(r));
				E_phi = (-1) .* ((1j.*k0.*I0)/(4*pi)) .* sin(theta) .* ( (1./r) + 1./(1j.*k0.*r.^2) ) .* E; 
			
			else %Use FF_Approximation
				
				E=1*exp(            -1j*obj.phase); %dummy term to include phase
							
				H_r		=zeros(1,numel(r))*E;
				H_theta =((1j.*k0.*I0)/(4*pi*eta0)) .* sin(theta)*E; 
				H_phi		= zeros(size(H_r))*E;

				E_r			=zeros(1,numel(r))*E;
				E_theta		=zeros(1,numel(r))*E;
				E_phi = (-1) .* ((1j.*k0.*I0)/(4*pi)) .* sin(theta) *E; 
			end
			
		end
		
		function [H_r, H_theta, H_phi, E_r, E_theta, E_phi, r_, phi_, theta_]=FieldInCart(obj, x,y,z)
			%Given x,y,z coordinates; compute the field
			T=classTransforms;
			[r, theta, phi]=T.cart2sph(x,y,z);
			%figure; hold on;
			%plot(r, '-b');
			%plot(phi.*T.RAD2DEG, '-r');
			%plot(theta*T.RAD2DEG, '-g');
			%legend('r', 'phi', 'theta');
			[H_r, H_theta, H_phi,E_r, E_theta, E_phi]=obj.FieldInSph(r, theta, phi);
			r_=r;
			phi_=phi;
			theta_=theta;
		end
	end
	
end

