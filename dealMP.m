function [ x,y,z] = dealMP( genMP,i)
% WORKHORSE function to return [x,y,z] values for any page of a 3D MP
% structure

x=genMP(1,:,i);
y=genMP(2,:,i);
z=genMP(3,:,i);

end

