function [ output_args ] = isAValidDipole( M)
%Check to see if M is a classDipole
output_args=strmatch(class(M), 'classMagneticDipole');
%Return a 1 if M is a dipole

%TODO: now that I see this, I am evidently checking only for Magnetic dipole, but I could check if it's an electric dipole here too. 
% I forget why this was done..TBH

end

